create proc sp_TextChange(@OriginalText varchar(500),@ReplaceText varchar(500))as
begin
	begin try
	begin transaction tr_TextChange
		--select REPLACE(Message,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText)))
		--,Message,REPLACE(Description,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText))),Description from Err_List_S 
		--select REPLACE(emailsubject,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText))),EmailSubject
		--,REPLACE(EmailBody,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText))),EmailBody
		--from Email_Content_D
		update Err_List_S set Message=REPLACE(Message,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText))),
		Description=REPLACE(Description,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText)))
		
		update Email_Content_D set EmailSubject=REPLACE(emailsubject,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText))),
		EmailBody=REPLACE(EmailBody,LTRIM(RTRIM(@OriginalText)),LTRIM(RTRIM(@ReplaceText)))
	commit transaction tr_TextChange
	end try
	begin catch
		rollback transaction tr_TextChange
		DECLARE @ERRORMESSAGE NVARCHAR(400),@ERRORSEVERITY INT,@ERRORSTATE INT          
		SELECT         
		@ERRORMESSAGE=ERROR_MESSAGE(),        
		@ERRORSEVERITY=ERROR_SEVERITY(),        
		@ERRORSTATE=ERROR_STATE()           
		RAISERROR(@ERRORMESSAGE,@ERRORSEVERITY,@ERRORSTATE)          
	end catch
end

--sp_TextChange 'Existing text','Required Text'