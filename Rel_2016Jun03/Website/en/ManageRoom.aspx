<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.	
*
* You should have received a copy of the RMT license with	
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_ManageRoom.ManageRoom" %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNet.aspx" -->
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }    
%>
<script language="javascript" src="inc/functions.js" type="text/javascript" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Manage Room</title>
<!-- For Location Issues - Script Code Moved in below Else loop-->
    <script language="JavaScript" type="text/javascript">
<!--
        //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
        //ZD 100604 End
	function frm_validate(){
	
		// room name
		if (document.getElementById("RoomName"))
		if (document.getElementById("RoomName").value != "") {
			
			if(checkInvalidChar(document.getElementById("RoomName").value) == false){
				return false;
			}
		}
}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End
//-->
    </script>

</head>
<body>
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
      
    
    <table width="95%" style="vertical-align:bottom" cellpadding="3" cellspacing="2">
    <tr>
        <td height="15px"> 
        </td>
    </tr>
    <tr>
    <td>
    <center>
        <h3 id="hMgHdg" runat="server" visible="True"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_hMgHdg%>" runat="server"></asp:Literal><!-- FB 2570 -->
            <asp:Label id="title" runat="server" text=""></asp:Label> 
            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, RoomCalendar_Rooms%>" runat="server"></asp:Literal>
        </h3>
        <h3 id="hSearchHdg" runat="server" visible="false">
            <asp:Label id="lblSearchHdg" runat="server" text="<%$ Resources:WebResources, ManageRoom_lblSearchHdg%>"></asp:Label>
        </h3>
        <div id="dataLoadingDIV" style="display:none" align="center" >
           <img border='0' src='image/wait1.gif'  alt='Loading..' />
        </div> <%--ZD 100678 End--%>
    </center>
    </td>
    </tr>
        <tr align="center">
            <td align="center">
                <asp:Label ID="LblError" CssClass="lblError" runat="server" 
                    Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <%--Edited FOr Location Issue--%>
    <form method="post" autocomplete="off" id="frmManageroom" name="frmManageroom" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <input name="settings2locstr" type="hidden" id="settings2locstr" runat="server" />
        <input name="settings2locpg" type="hidden" id="settings2locpg" runat="server" />
        <input name="getLocID" type="hidden" id="getLocID" runat="server" />
        <input name="sSession" type="hidden" id="sSession" runat="server" />
        <input type="hidden" id="MainLoc" runat="server" name="MainLoc" />
        <input id="helpPage" type="hidden" value="63" />
        <div>

<!--Location Issues Start-->            
<%
if(settings2locpg.Value == "settings2locfail.aspx?wintype=ifr")
{
%>
<!--Location Issues End-->
<br><br>
  <center><b><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_Therearenoro%>" runat="server"></asp:Literal></b></center>
  <br><br><br><br><br>
  <input type="hidden" name="cmd" value="GetNewRoom">

  <center>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td align="center"> 
          
        </td>
      </tr>
    </table>
  </center>
<!--Location Issues Start-->
<%
}
else
{
%>
            <script language="JavaScript" type="text/javascript">
<!--

	var RoomDisplayMod = ( ("<%=Session["RoomListView"]%>" == "list") ? 2 : 1 );
	
	var RoomCheckableMod = 1;
	
	
	
	function chgRoomDisplay(locpghref, mod, forced)
	{
	
	locpghref = "<%=settings2locpg.Value%>"
		var special;
		mod = parseInt(mod);
		if ((mod != RoomDisplayMod) || forced)   {
			RoomDisplayMod = mod;
			var tmpstr = "";
			if (ifrmLocation) {
			if(ifrmLocation.document.frmSettings2loc.selectedloc.value != "") // added for Location Issues
				tmpstr = ifrmLocation.document.frmSettings2loc.selectedloc.value;
				ctmpstr="";
				if (typeof(ifrmLocation.document.frmSettings2loc.comparedsellocs) != "undefined")
					ctmpstr = ifrmLocation.document.frmSettings2loc.comparedsellocs.value; }
				if (typeof(ifrmLocation.document.frmSettings2loc.special)!="undefined") {
					if (ifrmLocation.document.frmSettings2loc.special.value == "")
						special = "0";
					else
						special = ifrmLocation.document.frmSettings2loc.special.value;}
			ifrmLocation.location.href = locpghref + "&mod=" + mod + "&cursel=" + tmpstr + "&comp=" + ctmpstr + "&special=" + special + "&";
		}
	}


	function roomCheckable(needenable)
	{
	
		RoomCheckableMod = needenable;
		
		updateRoomCheckable();
		
	}


	function updateRoomCheckable()
	{
		els = ifrmLocation.document.frmSettings2loctree.elements;
		for (var i = 0; i < els.length; i++) {
			if (els[i].type == "checkbox") {
				els[i].disabled = !RoomCheckableMod;
			}
		}
		
	}

//-->
    </script>
<!--Location Issues End-->
            <input name="cmd" type="hidden" />
                <table border="0" cellpadding="4" cellspacing="6" width="98%">
                <tr>
                    <td colspan="3"> <%-- FB 2612 iframe height 633--%>
                         <iframe id="RoomFrame" onfocus="setTimeout('window.scrollTo(0,0);', 1000);" runat="server" width="100%" valign="top" height="798px" scrolling="no"></iframe> <%--NGC UI Issue--%><%--FB 2694--%><%--ZD 100420 ZD 101918--%> 
                    </td>
                </tr>
                
                    <tr style="display:none;">
                   
                        <td valign="top" style="width: 450">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <!-- -->
                                <tr>
                                    <td style="height: 20; width: 3%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>1</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td style="width: 96%" align="left">
                                        <span class="subtitleblueblodtext" id="spMgRooms" runat="server" visible="false"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_spMgRooms%>" runat="server"></asp:Literal></span><br />
                                        <span class="subtitleblueblodtext" id="spSearch" runat="server" visible="false"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_spSearch%>" runat="server"></asp:Literal></span><br />
                                       <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_Choosearoomt%>" runat="server"></asp:Literal></span> 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 15">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left">
                                        <font size="1" class="blackblodtext">
                                            <img height="15" src="image/deleted.gif" width="16" alt="ImgDel" style="cursor:pointer;" title="Delete" /> - <asp:Literal Text="<%$ Resources:WebResources, ManageRoom_RoomDeactivate%>" runat="server"></asp:Literal></font>
                                        <br />
                                        <font size="1" class="blackblodtext">
                                            <img height="16" src="image/locked.gif" width="15" alt="ImgLock" />
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>- Room Locked. Room is reserved for a hearing.<%}else{ %>- Room Locked. Room is reserved for a conference.<%} %> </font><%--Edited  For FB 1428--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" colspan="3" style="height: 30" valign="bottom">
                                        <table border="0" width="305">
                                            <tr>
                                                <td align="right">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <%--Window Dressing--%>
                                                            <td class="blackblodtext">
                                                                <input id="RdroomLevDisplayMod" name="roomListDisplayMod" onclick="JavaScript:chgRoomDisplay('',1);" type="radio" value="1" runat="server" /><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_LevelView%>" runat="server"></asp:Literal></td>
                                                            <td style="width: 10">
                                                            </td>
                                                             <%--Window Dressing--%>
                                                            <td class="blackblodtext">
                                                                <input id="RdroomListDisplayMod" name="roomListDisplayMod" onclick="JavaScript:chgRoomDisplay('',2);" type="radio" value="2" runat="server" /><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_ListView%>" runat="server"></asp:Literal></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td id="lociframe">
                                        <iframe align="left" height="350" name="ifrmLocation" src="<%=settings2locpg.Value%>" valign="top" width="305" id="ifrmLocation" runat="server">
                                            <p><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_goto%>" runat="server"></asp:Literal><a href="<%=settings2locpg.Value%>"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_LocationList%>" runat="server"></asp:Literal></a></p>
                                        </iframe>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left">
                                    <asp:Label id="lblTtlRooms" cssclass="blackblodtext" text="<%$ Resources:WebResources, ManageRoom_lblTtlRooms%>" runat="server"></asp:Label>
                                         <span class="summaryText">
                                            <asp:Label ID="totalNumber" runat="server"></asp:Label>;
                                        <asp:Label ID="Label1" CssClass="blackblodtext" Text="<%$ Resources:WebResources, ManageRoom_Label1%> " runat="server"></asp:Label>
                                            <asp:Label ID="ttlnvidLbl" runat="server"></asp:Label>
                                        <asp:Label ID="lblVMRRooms" CssClass="blackblodtext" Text="<%$ Resources:WebResources, ManageRoom_lblVMRRooms%>" runat="server"></asp:Label><%--FB 2586--%>
                                            <asp:Label ID="tntvmrrooms" runat="server"></asp:Label>
                                            </span>; &nbsp; <br /><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_LicensesRemain%>" runat="server"></asp:Literal> </span> <span class="summaryText">
                                            <asp:Label ID="licensesRemain" runat="server"></asp:Label>
                                        </span><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_VideoRoomsRem%>" runat="server"></asp:Literal></span> <span class="summaryText">
                                            <asp:Label ID="vidLbl" runat="server"></asp:Label>
                                        </span><span class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, ManageRoom_NonVideoRooms%>" runat="server"></asp:Literal> </span> <span class="summaryText">
                                            <asp:Label ID="nvidLbl" runat="server"></asp:Label>
                                        </span><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_VMRRoomsRemai%>" runat="server"></asp:Literal></span> <span class="summaryText"><%--FB 2586 ZD 100806--%>
                                            <asp:Label ID="vmrvidLbl" runat="server"></asp:Label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnDeleteRoom" CssClass="altShortBlueButtonFormat" OnClientClick="javascript:return ChkRoomNumValid(-1);"
                                            runat="server"  Text="<%$ Resources:WebResources, ManageRoom_btnDeleteRoom%>"></asp:Button>
                                        <asp:Button ID="btnEdit" CssClass="altShortBlueButtonFormat" OnClientClick="javascript:return ChkRoomNumValid(0);"
                                            runat="server"  Text="<%$ Resources:WebResources, ManageRoom_btnEdit%>"></asp:Button>
                                    </td>
                                </tr>
                                <!-- -->
                            </table>
                        </td>
                        <td style="width: 50" style="display:none;">
                            <table border="0" cellpadding="0" cellspacing="5" width="100%" style="display:none;">
                                <tr>
                                    <td align="center" valign="middle">
                                        <img height="260" src="image/aqualine.gif"  style="vertical-align:middle" width="2" alt="ImgAqua" /> <%--ZD 100419--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <font color="#00ccff" size="3"><b><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_OR%>" runat="server"></asp:Literal></b></font>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <img height="140" src="image/aqualine.gif" style="vertical-align:middle" width="2" alt="ImgAq" /> <%--ZD 100419--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 400" style="display:none;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none;">
                                <!-- -->
                                <tr>
                                    <td style="height: 20; width: 1%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>2</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td width="96%" align="left">
                                        <span class="subtitleblueblodtext" id="spSrhRm" runat="server" visible="false"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_spSrhRm%>" runat="server"></asp:Literal></span><br />
                                        <span class="subtitleblueblodtext" id="spNwRm" runat="server" visible="false"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_spNwRm%>" runat="server"></asp:Literal></span><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 15">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="right">
                                                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_RoomName%>" runat="server"></asp:Literal></span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRoomName" CssClass="altText" MaxLength="256" name="RoomName"
                                                        onkeyup="javascript:chkLimit(this,'2');" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_RoomCapacityG%>" runat="server"></asp:Literal></span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRoomCapacity" CssClass="altText" name="RoomCapacity" size="10"
                                                        type="text" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_Projector%>" runat="server"></asp:Literal></span>
                                                </td>
                                                <td align="left">
                                                <%--window dressing--%>
                                                    <asp:DropDownList ID="Projector" CssClass="altText" name="Projector" runat="server" AutoPostBack="false">
                                                        <asp:ListItem Selected="True" Value="-1" Text="any"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="yes"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="no"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 110">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                    <%--code added for Soft Edge button--%>                                                    
                                                    <input type="button" name="Reset" class="altShortBlueButtonFormat" value="Reset" onclick="javascript:fnResetValues(0);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                    <asp:Button id="btnSearchSubmit" cssclass="altShortBlueButtonFormat" name="ManageroomSubmit" text="<%$ Resources:WebResources, ManageRoom_btnSearchSubmit%>" runat="server"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 10">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" style="height: 2">
                                        <img height="2" src="image/aqualine.gif" style="vertical-align:middle" width="95%" alt="Img" /> <%--ZD 100419--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 20">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20; width: 1%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>3</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td style="width: 96%">
                                        <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_CreateaNewRo%>" runat="server"></asp:Literal></span><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 100">
                                    </td>
                                </tr>
                                
                                <!-- -->
                            </table>
                        </td>
                    </tr>
                    <tr>
                                    <td style="height: 20; width: 1%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>3</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td style="width: 96%">
                                        <!--<span class="subtitleblueblodtext"></span><br />--><%--Commented for FB 2094--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="right">
                                    <button  name="ManageroomSubmit" ID="btnNewSubmit" runat="server" style="width:350px">
											<asp:Literal Text="<%$ Resources:WebResources, ManageRoom_btnNewSubmit%>" runat="server"></asp:Literal></button><%-- FB 2694 FB 2796 --%> <%--ZD 100806--%>
                                        <button class="altShortBlueButtonFormat" name="ManageroomSubmit" style="display:none;" onclick="DataLoading(1);"
                                            ID="btnSubmit" runat="server"><asp:Literal Text="<%$ Resources:WebResources, ManageRoom_btnSubmit%>" runat="server"></asp:Literal></button><%--ZD 100176--%> 
                                    </td>
                                </tr>
                                <tr> <%-- FB 2448 --%>
                                   <td></td>
                                   <td></td>
                                   <td align="right">
                                      <button ID="btnCreateVMRRoom" runat="server" style="width:350px"  onclick="DataLoading(1);">
										<asp:Literal Text="<%$ Resources:WebResources, ManageRoom_btnCreateVMRRoom%>" runat="server"></asp:Literal></button> <%--FB 2796--%> <%--ZD 100176 ZD 100806--%> 
                                   </td>
                                </tr>
                                
                                <%--FB 2694 Starts--%>
                                <tr> 
                                   <td></td>
                                   <td></td>
                                   <td align="right">
                                      <button ID="btnHotdeskingRoom" runat="server" style="width:350px"  onclick="DataLoading(1);">
										<asp:Literal Text="<%$ Resources:WebResources, ManageRoom_btnHotdeskingRoom%>" runat="server"></asp:Literal></button> <%--FB 2796--%><%--ZD 100176--%>  <%--ZD 100806--%>
                                   </td>
                                </tr>
                                <%--FB 2694 End--%>                    
                </table>
            

            <script language="JavaScript" type="text/javascript">
<!--

if (typeof(ifrmLocation) != "undefined") {
tmpstr = "<%=getLocID.Value%>";
	switch ("<%= Session["RoomListView"] %>") {
		case "level":
			document.frmManageroom.roomListDisplayMod[0].checked = true;
			ifrmLocation.location.href = "<%=settings2locpg.Value%>" + "&mod=1&cursel=" + tmpstr + "&";
			break;
//			alert("<%=settings2locpg.Value%>");
		case "list":
			document.frmManageroom.roomListDisplayMod[1].checked = true;
			ifrmLocation.location.href = "<%=settings2locpg.Value%>" + "&mod=2&cursel=" + tmpstr + "&";
			break;
	}
}


//-->
            </script>

            
<%
}
%>

        </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<script language="JavaScript" type="text/javascript">
<!--
function ChkRoomNumValid (opr)
{
	var t = (s = ifrmLocation.document.frmSettings2loc.selectedloc.value).substring (2, s.length);
	
	rn = t.split(", ").length - 1;
	
	switch (opr) 
	{
		case -1:
			switch (rn) 
			{
				case 0:
				    alert(ManageRoomConf)
					return false;
					break;
				default:
				    var isRemoveRms = confirm(ManageRoomDelete)
					if (isRemoveRms == false) 
					{
						return (false);
					}
					document.frmManageroom.cmd.value = "DeleteRoom";
					break;				
			}
			break;
		case 0:
			switch (rn) 
			{
				case 0:
				    alert(ManageRoomEdit)
					return false;
					break;
				case 1:
					document.frmManageroom.cmd.value = "GetOldRoom";
					break;
				default:
				    alert(ManageOnlyRoomEdit)
					return false;
					break;				
			}
			break;
	}
   document.frmManageroom.MainLoc.value = t;
   document.frmManageroom.submit();
}
//-->
</script>

