/*ZD 100147 Start*/
/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/
/*ZD 100147 ZD 100886 End*/

var ie = Boolean(document.all);
   var ns4=Boolean(document.layers && !document.getElementById);
   var gecko=Boolean(!document.all && document.getElementById);
   
   function disableclick(e) {
       if (ns4) {
           if (e.which == 3) return false;
       } else if (gecko){
           if (e.button==2){
               e.cancelBubble=true;
               e.stopPropagation();
               e.preventDefault();
               return false;
           }
       }
   }
   if (ie) document.oncontextmenu=function() { return false; }
   else if (ns4){
       document.onmousedown=disableclick
       if (ns4) document.captureEvents(Event.MOUSEDOWN);
   } else {
       document.addEventListener('click',disableclick,false)
       document.addEventListener('dblclick',disableclick,false)
   }