<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_endpointdetails" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Endpoint Details</title>  
<script runat="server">
</script>
<%--FB 2790 Starts--%>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>   
<%--FB 2790 Ends--%>

<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
 <script type="text/javascript" language="javascript">
 //FB 2400
     function toggleDiv(id, flagit)
     {
         if (flagit == "1") 
          document.getElementById("multiCodecPopUp").style.display = 'block';
         else if (flagit == "0") 
          document.getElementById("multiCodecPopUp").style.display = 'none';
     }
 </script>
</head>
<body>
<form runat="server" id="frmEndpointDetails">
 <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
<%--FB 2400 start--%>
<div id="multiCodecPopUp"  runat="server" align="center" style="top: 150px;left:365px; POSITION: absolute; WIDTH:30%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none; background-color:#E0E0E0"> <%--ZD 100426--%> 
      <table cellpadding="2" cellspacing="1"  width="70%" class="tableBody" align="center">
         <tr>
            <td class="subtitleblueblodtext" align="center" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_Address%>" runat="server"></asp:Literal></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="multicodec" runat="server"></asp:Label>               
            </td>
        </tr>
      </table>
</div>
<%--FB 2400 end--%>

<div>
       <table width="96%" >
        <tr>
            <td align="center">
                <h3><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_EndpointDetail%>" runat="server"></asp:Literal></h3>
            </td>
        </tr>  
        <tr>
            <td align="center" style="width: 1168px">
                <asp:Label ID="ErrLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
            </td>
        </tr>              
        </table> 
        <table border="0" cellpadding="2" cellspacing="2" width="95%">     
         <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_EndpointName%>" runat="server"></asp:Literal></td> 
            <td align="left">
                <asp:Label ID="LblName" runat="server"></asp:Label>
            </td>
         </tr> 
         <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_Password%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblPass" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_AddressType%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblAddrType" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_Address%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblAddr" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_Model%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblMdl" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_PreferredDiali%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblPreDialingOpn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_PreferredBandw%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblPreBandWidth" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_AssignedtoMCU%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblAssToMcu" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_Locatedoutside%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblLocatedOutsideNet" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_WebAccessURL%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblWebAccURL" runat="server"></asp:Label>
            </td>
        </tr>
       <%-- Code Added For FB 1422--%>
         <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_TelnetAPIEnab%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblTelnet" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
         <%--Window Dressing--%>
            <td align="left" style="width:54%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, endpointdetails_EmailID%>" runat="server"></asp:Literal></td>
            <td align="left">
                <asp:Label ID="LblExchange" runat="server"></asp:Label>
            </td>
        </tr>
        </table>
  
    <br />
      <table border="0" cellpadding="1" cellspacing="0" width="100%">
        <tr>
            <td align="center">
                    <%--code added for Soft Edge button--%>
                <input type="button" name="Close" onclick="Javascript: window.close();" value="<%$ Resources:WebResources, Close%>" runat="server" class="altMedium0BlueButtonFormat"/>           
            </td> 
        </tr>
      </table>
    </div>
   </form>
  </body>
</html>
<%--code added for Soft Edge button--%>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script type="text/javascript" language="javascript">
    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {

            window.close();

        }
    }
 </script>
 <%--ZD 100428 END--%>
<script type="text/javascript" src="inc/softedge.js"></script>