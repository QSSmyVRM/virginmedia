<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.	
*
* You should have received a copy of the RMT license with	
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.ManageOrganization" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript" src="inc/functions.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script language="javascript" type="text/javascript">
        //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
        //ZD 100604 End
//        function FnCancel()
//		{
//		    DataLoading(1); // ZD 100176
//			window.location.replace('SuperAdministrator.aspx');
//		}
		//ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
		    else
		        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
		}
		//ZD 100176 End
    </script>
    <title>Manage Organization</title>
</head>
<body>
    <form id="frmManageOrganization" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    
    <div>
        <table cellpadding="0" cellspacing="0" id="OuterTable" width="100%" border="0">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, ManageOrganization_lblHeader%>"></asp:Label><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" style="display:none" align="center" >
                         <img border='0' src='image/wait1.gif'  alt='Loading..' />
                    </div> <%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgOrganizations" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" OnEditCommand="EditOrganizationProfile" OnDeleteCommand="DeleteOrganizationProfile" OnCancelCommand="PurgeNow_Click"  OnItemDataBound="BindRowsDeleteMessage" ShowFooter="false" Width="90%" Visible="true" style="border-collapse:separate"><%-- Edited for FF--%> <%--FB 1753 //FB 2074--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="orgId" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn> 
                            <asp:BoundColumn DataField="organizationName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, OrganizationName%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="phone" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ManageOrganization_Phone%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="emailID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ManageOrganizationProfile_EmailID%>"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <table width="50%" align="center">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="<%$ Resources:WebResources, ManageOrganization_lnkEdit%>" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton>  <%--ZD 100176--%> 
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="<%$ Resources:WebResources, Delete%>" CommandName="Delete" Enabled='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>' 
                                                ></asp:LinkButton> <%--ZD 100176--%> <%--ZD 100429--%> 
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkPurge" runat="server" Text="<%$ Resources:WebResources, ManageOrganization_lnkPurge%>" CommandName="Cancel"  Enabled='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>' 
                                                ></asp:LinkButton><%--ZD 100176--%> <%--ZD 100429--%> 
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoOrganizations" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, NoOrgFound%>" runat="server"></asp:Literal>  <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <table cellspacing="0" cellpadding="0" width="90%" border="0" align="center">
                        <tr>
                            <td width="60%">&nbsp;</td>
                            <td class="blackblodtext" align="right" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, ManageOrganization_TotalOrganizat%>" runat="server"></asp:Literal><span id="SpnActiveOrgs" runat="server" class="blackblodtext"></span>
                            </td>
                            <td class="blackblodtext" align="right" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, ManageOrganization_LicenseRemaini%>" runat="server"></asp:Literal><span id="SpnLicense" runat="server" class="blackblodtext"></span>
                            </td>
                        </tr>
                    </table>                    
                </td>
            </tr>
            <tr>
                <td align="center" width="90%"><br />
                    <table cellspacing="0" cellpadding="1" width="90%" border="0">
                        <tr>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Create New Organization</SPAN>--%>  <%--ZD 100926--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><br />
                    <table cellspacing="0" cellpadding="3" width="90%" border="0" align="center">
                        <tr >
                            <td align="right">
                                <%--<%--<%--<input class="altMedium0BlueButtonFormat" id="btnCancel" onclick="FnCancel()" type="button" value="Cancel" name="btnCancel"></><%--ZD 100428--%>
                                <button id="btnCancel" class="altMedium0BlueButtonFormat" runat="server" onserverclick="FunctionCancel" onclick="javascript:DataLoading('1');"><asp:Literal Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
                                <%--<asp:Button ID="btnNewOrganization"  Width="100pt" runat="server"  Text="Submit" OnClick="CreateNewOrganization"  OnClientClick="javascript:DataLoading('1');"/> <%--FB 2664--%> <%--ZD 100176--%> <%--ZD 100176--%><%--ZD 100420--%>
								<button ID="btnNewOrganization" style="width:220px;" class="altLongBlueButtonFormat" runat="server" onserverclick="CreateNewOrganization" onclick="javascript:DataLoading('1');" ><asp:Literal Text="<%$ Resources:WebResources, ManageOrganization_CreateNewOrga%>" runat="server"></asp:Literal></button><%--ZD 100420--%>  <%--ZD 100926--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         <script type="text/javascript" src="inc/softedge.js"></script>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
    //document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnNewOrganization').focus()"); //ZD 100420
</script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
