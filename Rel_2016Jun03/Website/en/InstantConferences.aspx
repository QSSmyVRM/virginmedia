<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%><%--ZD 100147 End--%>

<%@ Page Language="C#" Inherits="en_InstantConference.InstantConference" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script type="text/javascript" src="script/settings2.js"></script><%--ZD 102587--%>
<script type="text/vbscript" src="script/settings2.vbs"></script>

<script type="text/javascript">

    function getYourOwnEmailList() {
        
        var url = "emaillist2main.aspx?t=e&frm=party2NET&fn=Setup&n=";

        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470px,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470px ,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=920ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }

    function ClosePopUp() {
        window.parent.document.getElementById('modalDiv').style.display = 'none';
        window.parent.document.getElementById('contentDiv').style.display = 'none';

    }


    function DataLoading(val) {
        if (val == "1") {
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
            document.body.style.cursor = "wait";
        }
        else {
            document.getElementById("dataLoadingDIV").style.display = 'none';  //ZD 100678
            document.body.style.cursor = "none";
        }
    }
</script>

<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Instant Conferences</title>
</head>
<body>
    <form id="frmInstantConference" autocomplete="off" runat="server" method="post"><%--ZD 101190--%>
    <input runat="server" name="hdnstatic" id="hdnstatic" type="hidden" />
    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                     </div>
    <div>
        <table width="80%" align="center">
            <tr> <%--ZD 101714--%>
                <td align="center" colspan="2">
                    <b><font class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, TrueDaybookInstantConference%>" runat="server"></asp:Literal></font></b><br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <b><font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, InstanceConference_Lbl1%>" runat="server"></asp:Literal></font></b>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <b><font class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, InstanceConference_Lbl2%>" runat="server"></asp:Literal></font></b><br>
                    <br>
                </td>
            </tr>
            <tr>
                <br />
                <br />
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <table width="80%" border="0">
                        <tr>
                            <td align="center">
                                <asp:Label ID="errLabel" runat="server" CssClass="lblError" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="25%">
                    <asp:Label runat="server" ID="lbltxt" CssClass="blackblodtext" Text="<%$ Resources:WebResources, VirtualRoom%>"></asp:Label>
                </td>
                <td width="50%" align="left">
                    <asp:DropDownList ID="lstStaticDynamic" Width="400px" runat="server" DataValueField="Id"
                        DataTextField="Name">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">
                    <asp:Label runat="server" ID="Label1" CssClass="blackblodtext" Text="<%$ Resources:WebResources, InviteParticipants%>"></asp:Label><%--ZD 101226--%>
                   &nbsp;
                   <img id="Img5" onclick="javascript:getYourOwnEmailList()" src="image/edit.gif" style="cursor: pointer;" runat="server"
                        title="<%$ Resources:WebResources, myVRMAddressBook%>" />
                </td>
                <td align="left">                   
                    <asp:TextBox ID="txtInstantConferenceParticipant" TextMode="MultiLine" runat="server"
                        CssClass="altText" Width="400px"></asp:TextBox>  <br>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="btnConfSubmit" runat="server" 
                        Text="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" OnClick="SetConference" class="altMedium0BlueButtonFormat" OnClientClick="DataLoading(1);" />
                    <asp:Button ID="btnClose" OnClientClick="ClosePopUp();" runat="server" Text="<%$ Resources:WebResources, Cancel%>"
                        class="altMedium0BlueButtonFormat" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">
    document.getElementById("txtInstantConferenceParticipant").value = ""; // ZD 100167
</script>

