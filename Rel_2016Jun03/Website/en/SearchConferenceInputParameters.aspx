<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.		
*
* You should have received a copy of the RMT license with	
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_SearchConference.SearchConference" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2968 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2968 Ends -->--%>

<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- Window Dressing -->
 
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script><%--Added For 1420--%>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%>  <%--FB 1982--%>


<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript" src="script/RoomSearch.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>

<%--ZD 102909 Start--%>
<style type="text/css">
    
.completionList 
{
    border:solid 1px Gray;
    margin:0px;
    padding:3px;
    height: 120px;
    overflow:auto;
    background-color: #FFFFFF;     
} 
        
.listItem 
{
    color: #191919;
} 
        
.itemHighlighted
{
    background-color: #ADD6FF;       
}
        
.loadingImg
{
    background-image: url(image/wait1.gif);
    background-position: right;
    background-repeat: no-repeat;
}        

</style>
<%--ZD 102909 End--%>

<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
//ZD 100429
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100369
function PopupWindow() {
    var popwin = window.showModalDialog("BridgeList.aspx", document, "resizable: yes; scrollbar: no; help: yes; status:yes; dialogHeight:580px; dialogWidth:510px");
}

//ZD 102909 - Start
function OnClientPopulating(sender, e) {
    sender._element.className = "loadingImg";
}
function OnClientCompleted(sender, e) {
    sender._element.className = "";
}

function validate(par) {
    if (par != null)
    {
        document.getElementById('hdnSelEntityCodeVal').value = par.value;

        var custId = par.id.split('_')[2];
        var regExpid = "ValCA21_" + custId;

        document.getElementById('hdnEntityRegID').value = regExpid;

    }    
}
//ZD 102909 - End

function fnShow()
{
    var chkHotdesk = document.getElementById("chkHotdesk")
    
    if(chkHotdesk.checked) {
        document.getElementById("tblSelectMCU").style.display = "None";
        document.getElementById("trSaveHeading").style.display = "None";
        document.getElementById("trSaveGrid").style.display = "None";
        document.getElementById("trHost").style.display = "None";
        document.getElementById("trParti").style.display = "None";
        document.getElementById("trParti").style.display = "None";
        if(document.getElementById("trSaveSearch"))
            document.getElementById("trSaveSearch").style.display = "None";
        document.getElementById("btnSaveSearch").style.display = "None";

        document.getElementById("trFirstname").style.display = "";
        document.getElementById("trLastName").style.display = "";
        document.getElementById("trEmail").style.display = "";
        document.getElementById("tdReservationDate").style.display = "";
        document.getElementById("tdConferenceDate").style.display = "None";
        document.getElementById("tdConfirmationNumber").style.display = "";
        document.getElementById("tdConferenceID").style.display = "None";
        document.getElementById("tdConfName").style.display = "None";
        document.getElementById("tdTxtConfName").style.display = "None";
        document.getElementById("tdSearchconfsubtitle").style.display = "None";
        document.getElementById("tdSearchsubtitle").style.display = "";
        document.getElementById("trConcierge").style.display = "none"; //FB 3007
        
        if (document.getElementById('btnReset') != null) { //ZD 100369
            document.getElementById('btnReset').removeAttribute("onblur", "");
            document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnSubmit').setAttribute('onfocus', '');document.getElementById('btnSubmit').focus();");
        }          
    }
    else {

        document.getElementById("tblSelectMCU").style.display = "";
        document.getElementById("trSaveHeading").style.display = "";
        document.getElementById("trSaveGrid").style.display = "";
        document.getElementById("trHost").style.display = "";
        document.getElementById("trParti").style.display = "";
        document.getElementById("trParti").style.display = "";
        document.getElementById("btnSaveSearch").style.display = "";
        
        document.getElementById("trFirstname").style.display = "None";
        document.getElementById("trLastName").style.display = "None";
        document.getElementById("trEmail").style.display = "None";
        document.getElementById("tdReservationDate").style.display = "None";
        document.getElementById("tdConferenceDate").style.display = "";
        document.getElementById("tdConfirmationNumber").style.display = "None";
        document.getElementById("tdConferenceID").style.display = "";
        document.getElementById("tdSearchconfsubtitle").style.display = "";
        document.getElementById("tdConfName").style.display = "";
        document.getElementById("tdTxtConfName").style.display = "";
        document.getElementById("tdSearchsubtitle").style.display = "None";
        document.getElementById("trConcierge").style.display = ""; //FB 3007
        if (document.getElementById('btnSaveSearch').style.display != 'none') {    //ZD 100369         
            document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnSaveSearch').setAttribute('onfocus', '');document.getElementById('btnSaveSearch').focus();");
            document.getElementById('btnSaveSearch').setAttribute("onblur", "document.getElementById('btnSubmit').setAttribute('onfocus', ''); document.getElementById('btnSubmit').focus()");
        }
    }
    
}

function SaveSearch(val)
{
    //Added for FB 1420 --  Start
    var confenddate = '';
    confenddate = GetDefaultDate(document.getElementById("txtDateTo").value,'<%=format%>');
    var confstdate = '';
    confstdate = GetDefaultDate(document.getElementById("txtDateFrom").value,'<%=format%>');

    //FB 2632 - Starts
    if(document.getElementById("chkDedicatedVNOCOperator")) //FB 2670
    {
        if (document.getElementById("chkDedicatedVNOCOperator").checked) {
            if (document.getElementById("hdnVNOCOperator").value == "") {
                alert(SelectVNOC);
                return false;
            }
        }
    }
    //FB 2632 - End
    
    if (Date.parse(confstdate) > Date.parse(confenddate))
    {
        alert(DateValidation);
             return false;
    }
    
    if(document.getElementById("trSaveSearch").style.display != "none" && document.getElementById("txtSearchTemplateName").value == "")
    {
        alert(SearchNameValidation);
        return false;
    }
    
    //Edited for FB 1420 -- End
    if (val == '1') // from edit
    {
        document.getElementById("trSaveSearch").style.display="";
        document.getElementById("btnSubmit").disabled = true;
        document.getElementById("txtSearchTemplateID").style.display = "none";
	    return true;
    }
    else if (val == "2") //from Search Button
    {
//        if ('<%=Session["EnableEntity"]%>' == '1')//FB 2607
//          {
//              if(document.getElementById("txtSearchTemplateName").value != "") 
//	            alert("Custom option values cannot be saved in a Template.");
//	      }
	    document.getElementById("txtSearchTemplateID").style.display = "none";
	    if (document.getElementById("trSaveSearch").style.display == "")
	    {
		    document.getElementById("trSaveSearch").style.display="none";
		    return true;
            }
	    else
	    {
	        document.getElementById("txtSearchTemplateID").value = "new";
		    document.getElementById("trSaveSearch").style.display="";
		    return false;
	    }
	   
    } 
    else
	return false;
	
	
}

function changeRoomSelection(objValue)
{
    //alert(objValue);
    if(objValue == "2")
    {
        document.getElementById("trRooms").style.display = "";
        //document.getElementById("trRooms1").style.display = "";
    }
    else
    {
        document.getElementById("trRooms").style.display = "none";
       // document.getElementById("trRooms1").style.display = "none";
    }
}
//100369 start
function changeMCUSelection(objValue) {
    if (objValue == "1") {
        document.getElementById("trSelectMCU").style.display = "";
    }
    else {
        document.getElementById("trSelectMCU").style.display = "none";
        document.getElementById("txtSelectedMCU").value = "";
    }
}
//100369 End
function changeDateSelection(objvalue)
{
    if(objvalue == "5")
    {
        document.getElementById("trDateFromTo").style.display = "";
        Page_ValidationActive=true;    }
    else
        document.getElementById("trDateFromTo").style.display = "none";
}

//Edited for FB 1420 -- Start
function ChangeEndDate(frm)
{
    var confstdate = '';
    confstdate = GetDefaultDate(document.getElementById("txtDateFrom").value,'<%=format%>');
    var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("txtDateTo").value,'<%=format%>');
       
    if (Date.parse(confenddate) > Date.parse(confstdate) )
        {
            if (frm == "0")
            {
                alert(RSdateFromLess);
                return false;
            }
        }
        else
        return true;
}

function ChangeStartDate(frm)
{
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("txtDateTo").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("txtDateFrom").value,'<%=format%>');
        
        if (Date.parse(confstdate) > Date.parse(confenddate))
        {
            if (frm == "0") 
            {
                alert(DateValidation);
                 return false;
            }
        }
        else
        return true;
}

//Edited for FB 1420 --  End

//FB 2632 - Starts
function getYourOwnEmailList(i) {
    url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i;

    if (!window.winrtc) {
        winrtc = window.open(url, "", "width=950,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
        winrtc.focus();
    }
    else if (!winrtc.closed) {
        winrtc.close();
        winrtc = window.open(url, "", "width=950,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
        winrtc.focus();
    }
    else {
        winrtc = window.open(url, "", "width=950ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
        winrtc.focus();
    }
}
function getVNOCEmailList()//FB 2670
{
    var chkAllSilo = "";
    if(document.getElementById("chkAllSilo") != null)
        chkAllSilo = document.getElementById("chkAllSilo"); //FB 2766
        
    var Confvnoc = document.getElementById("hdnVNOCOperator");
    if (chkAllSilo.checked == true)//FB 2766
        url = "VNOCparticipantlist.aspx?frm=1&cvnoc=" + Confvnoc.value; 
    else
        url = "VNOCparticipantlist.aspx?cvnoc=" + Confvnoc.value; 
    winrtc = window.open(url, "", "width=950,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2735
}

function deleteVNOC() {
    document.getElementById('txtVNOCOperator').value = "";
    document.getElementById('hdnVNOCOperator').value = "";
}
//FB 2632 Ends
//FB 2728 Starts
function fnCheck() {
    var chkAllSilo = document.getElementById("chkAllSilo");
    var hdnChkSilo = document.getElementById("hdnChkSilo");

    if (chkAllSilo) {
        if (chkAllSilo.checked == true)
            hdnChkSilo.value = "1";
        else
            hdnChkSilo.value = "0";
    }
}
//FB 2728
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Search Conference</title>
</head>
<body>
    <form id="frmSearchConference" runat="server" method="post">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
     <input type="hidden" runat="server" id="hdnEntityRegID" name="hdnEntityRegID" /> <%--ZD 102909--%>
     <input type="hidden" runat="server" id="hdnSelEntityCodeVal" name="hdnSelEntityCodeVal" /> <%--ZD 102909--%>
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center" style="height: 23px">
                    <h3><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_SearchHearing%>" runat="server"></asp:Literal><%}else{ %><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_lblTitle%>" runat="server"></asp:Literal><%} %></h3><br /> <%--Edited for FB 1428--%>
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                    </div><%--ZD 100678 End--%>
                     <asp:Label ID="errLabel" runat="server" Visible="False" CssClass="lblError"></asp:Label></td>
            </tr>
            <%--FB 2694--%><%--FB 2968--%> 
            <tr id="trHotdeskingSearch" runat="server">
                <td class="subtitleblueblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_HotdeskingSear%>" runat="server"></asp:Literal></td>
            </tr>
            <tr id="trchkHotdesk" runat="server"><%--FB 2968--%>
                <td align="left">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox id="chkHotdesk" runat="server" text="<%$ Resources:WebResources, SearchConferenceInputParameters_chkHotdesk%>" onclick="javascript:fnShow();"></asp:CheckBox>
                </td>
            </tr>
            <tr id="trSaveHeading"><%--FB 2694--%>
                <td align="left">
                    <asp:Label ID="Label1" runat="server" CssClass="subtitleblueblodtext"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_SavedSearches%>" runat="server"></asp:Literal></asp:Label></td>
            </tr>
            <tr id="trSaveGrid"><%--FB 2694--%>
                <td align="center">
                <asp:DataGrid ID="dgScheduledSearches" runat="server" AutoGenerateColumns="False" CellPadding="5" BorderStyle="none" CellSpacing="0"
                 Width="50%" OnItemCreated="BindRowsDeleteMessage"
                 OnEditCommand="EditSearchTemplate" OnCancelCommand="SearchConferenceFromTemplate" OnDeleteCommand="DeleteSearchTemplate">
                    <AlternatingItemStyle CssClass="tableBody"/>
                    <HeaderStyle CssClass="tableHeader" Height ="30"/>
                    <SelectedItemStyle BackColor="Orange" />
                    <ItemStyle CssClass="tableBody"/>
                    <Columns>
                        <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                        <%--Window Dressing--%>
                        <asp:BoundColumn DataField="name" HeaderText="<%$ Resources:WebResources, SearchConferenceInputParameters_Name%>" ItemStyle-CssClass="tableBody" ItemStyle-Width="60%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                            <HeaderStyle CssClass="tableHeader"/>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>">
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:LinkButton Text="<%$ Resources:WebResources, SearchConferenceInputParameters_btnSearchNow%>" runat="server" ID="btnSearchNow" OnClientClick="javascript:DataLoading(1);" CommandName="Cancel"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:LinkButton Text="<%$ Resources:WebResources, SearchConferenceInputParameters_btnEdit%>" OnClientClick="javascript:DataLoading(1);return SaveSearch('1')" runat="server" ID="btnEdit" CommandName="Edit"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:LinkButton Text="<%$ Resources:WebResources, SearchConferenceInputParameters_btnDelete%>" runat="server" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <ItemStyle Height="20px" />
                
                </asp:DataGrid>
                    <asp:Label ID="lblNoSearchTemplates" runat="server"
                        Text="<%$ Resources:WebResources, SearchConferenceInputParameters_lblNoSearchTemplates%>" Visible="False" CssClass="lblError"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr><%--FB 2694--%>
                <td id="tdSearchconfsubtitle" align="left">
                        <asp:Label ID="lblSearch1" runat="server" CssClass="subtitleblueblodtext"><%if(Application["Client"].ToString().ToUpper() == "MOJ")%><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_QuickSearch3%>" runat="server"></asp:Literal><%else%><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_QuickSearch1%>" runat="server"></asp:Literal></asp:Label> <%--Edited for FB 1428 MOJ--%>                        
                </td>
                <td id="tdSearchsubtitle" align="left" style="display:none">
                    <asp:Label ID="lblSearch2" runat="server" CssClass="subtitleblueblodtext"><%if(Application["Client"].ToString().ToUpper() == "MOJ")%><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_QuickSearch3%>" runat="server"></asp:Literal><%else%><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_QuickSearch2%>" runat="server"></asp:Literal></asp:Label> <%--Edited for FB 1428 MOJ--%>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" cellpadding="2" cellspacing="2" border="0" style="margin-left:20px"> <%-- FB 2050 --%>
                        <tr style="height:25px">
                            <%--Added for FB 1428 Start--%>
                                <% if (Application["Client"].ToString().ToUpper() == "MOJ")
                               {
                                %>
                                <td width="10%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_HearingID%>" runat="server"></asp:Literal></td>
                                <%}%>
                                <% else
                               { %>
                                <td id="tdConferenceID" width="11%" align="left" valign="top" class="blackblodtext"><%--FB 2694--%>
                                    <asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_tdConferenceID%>" runat="server"></asp:Literal>
                                </td>
                                <td id="tdConfirmationNumber" width="11%" align="left" valign="top" class="blackblodtext"  style="display:none";>
                                    <asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_tdConfirmationNumber%>" runat="server"></asp:Literal>
                                </td>
                                <% } %>
                                <%--Added for FB 1428 End--%>
                            <td align="left" width="25%" >
                                <asp:TextBox ID="txtConferenceUniqueID" runat="server" MaxLength="9" CssClass="altText" Text=""></asp:TextBox> <%--FB 2870 --%>
                                <%--ZD 100369--%>
                                <button id="btnSubmitID" type="button" ValidationGroup="ID" onserverclick="SubmitSearch" class="altMedium0BlueButtonFormat" runat="server" ><asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                                <%--<asp:Button id="btnSubmitID" CssClass="altMedium0BlueButtonFormat" Text="Submit" OnClientClick="javascript:DataLoading(1);" 
                                OnClick="SubmitSearch" ValidationGroup="ID" runat="server" Width="80px" />--%>
                                <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="txtConferenceUniqueID" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ValidationGroup="ID"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="reg1" runat="server" ControlToValidate="txtConferenceUniqueID" ValidationGroup="ID" ErrorMessage="<%$ Resources:WebResources, Numericonly%>" ValidationExpression="\d+" Display="dynamic"></asp:RegularExpressionValidator>
                            </td>
                            <%--TCK  #100037 Starts--%>
                            <td> 
                            <table>
                            <tr>
                            <td align="left" style="width:93px" nowrap="nowrap" runat="server" id="td1">
                            <asp:Label ID="Label5" runat ="server" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Label5%>" class="blackblodtext"></asp:Label></td>
                            <td><asp:CheckBox Height="17px" ID="chkDeleted" runat="server"/> 
                            </td></tr></table></td> 
                            <%--TCK  #100037 Starts--%>
                        </tr>
                        <%--FB 2870 Starts--%>
                        <tr id="trCTSNumericID" style="height:25px" runat="server">
                            <td width="10%" align="left" valign="top" class="blackblodtext" ID="trCTSNum">
                                   <asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_trCTSNum%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left" width="25%" >
                                <asp:TextBox ID="txtNumericID" runat="server" CssClass="altText"  Text=""></asp:TextBox>
                                 <%--ZD 100369--%>
                                <button id="btnNumericID" type="button" onserverclick="SubmitSearch" class="altMedium0BlueButtonFormat"  validationgroup="ID1" runat="server" onclick="javascript:DataLoading(0);"  ><asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                                <%--<asp:Button id="btnNumericID" CssClass="altMedium0BlueButtonFormat" Text="Submit" OnClick="SubmitSearch" ValidationGroup="ID1"  OnClientClick="javascript:DataLoading(1);"  runat="server" Width="80px" />--%>
                                <asp:RequiredFieldValidator ID="ReqNumericID" runat="server" ControlToValidate="txtNumericID" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ValidationGroup="ID1"></asp:RequiredFieldValidator>
                            </td>
                             <%--FB 2728 Starts--%>
                            <td> 
                            <table>
                            <tr>
                            <td align="left" style="width:93px" nowrap="nowrap" runat="server" id="tdchkSilo"><%--2843--%> 
                            <asp:Label ID="Label6" runat ="server" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Label6%>" class="blackblodtext"></asp:Label></td> <%--2843--%>
                            <td><asp:CheckBox Height="17px" ID="chkAllSilo" runat="server" OnClick="fnCheck()" AutoPostBack="true"/>  <%--FB 2382--%><%--2843--%>                             
                            </td></tr></table></td> 
                            <%--FB 2728 Ends--%>
                        </tr>
                        <%--FB 2870 Ends--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" CssClass="subtitleblueblodtext" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Label2%>"></asp:Label>
                </td>
            </tr>            
            <tr>
                <td align="left"> <%-- FB 2050 --%>
                    <table width="90%" cellpadding="2" cellspacing="2" border="0" style="margin-left:20px">                    
                        <tr>
                            <%--Added for FB 1428 Start--%>
                            <% if (Application["Client"].ToString().ToUpper() == "MOJ")
                               {
                            %>
                            <td align="left" class="blackblodtext" style="width: 10%"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_HearingName%>" runat="server"></asp:Literal></td>
                            <%}%>
                            <% else
                               { %>
                            <td id="tdConfName" align="left" class="blackblodtext" style="width: 12%"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_tdConfName%>" runat="server"></asp:Literal></td>
                            <% } %>
                            <%--Added for FB 1428 End--%>
                            <td id="tdTxtConfName" align="left" style="width:40%">
                                <!--[Vivek: 29th Apr 2008]Changed Regular expression as per issue number 306-->
                                <asp:TextBox ID="txtConferenceName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                                             <%-- Code Added for FB 1640--%>                                                
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtConferenceName" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%--FB 2321--%>
                            </td>
                             <%--ZD 100369 Start--%>
                        
                        <td>
                            <table id="tblSelectMCU">
                                <tr>
                                 <td align="left" valign="top" class="blackblodtext">
                                    <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, MCU%>" runat="server"></asp:Literal>
                                </td>
                                </tr>
                                <tr>
                                <td align="left" class="blackblodtext">
                                    <asp:RadioButtonList ID="rdMCUOption" runat="server" RepeatDirection="Horizontal" CssClass="blackblodtext"
                                        RepeatLayout="Flow">
                                        <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, Any%>" Value="0"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Selected%>"
                                            Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                </tr>
                                <tr id="trSelectMCU"  style="display: none">
                                        <td valign="top" align="left">
                                            <table border="0">
                                                <tr>
                                                    <td align="left" valign="top" style="width: 10%">
                                                    <%--<button id="btnSelectMCU" name="opnMCU" validationgroup="uplad" class="altMedium0BlueButtonFormat" >Select MCU</button>--%>
                                                        <input name="opnMCU" runat="server" type="button" id="btnSelectMCU" style="width:155px;"
                                                            value="<%$ Resources:WebResources, SearchConferenceInputParameters_SelectMCU%>" class="altMedium0BlueButtonFormat" />
                                                        <br />
                                                    </td>
                                                    <td align="left" valign="top" style="width: 90%">
                                                    <asp:TextBox ID="txtSelectedMCU" Enabled="false" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                 </tr>
                            </table>
                            </td>
                        <%--ZD 100369 End--%>
                        </tr>
                        <tr>
                            <%--Added for FB 1428 Start--%>
                            <% if (Application["Client"].ToString().ToUpper() == "MOJ")
                               {
                            %>
                            <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_HearingDate%>" runat="server"></asp:Literal></td>
                            <%}%>
                            <% else
                               { %>
                            <td id="tdConferenceDate" align="left" valign="top" class="blackblodtext"><%--FB 2694--%>
                                <asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_tdConferenceDate%>" runat="server"></asp:Literal>
                            </td>
                            <td id="tdReservationDate" align="left" valign="top" class="blackblodtext" style="display:none";>
                                <asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_tdReservationDate%>" runat="server"></asp:Literal>
                            </td>
                            <% } %>
                            <%--Added for FB 1428 End--%>
                            <td align="left">
                                <table cellpadding="2" cellspacing="2" width="100%">
                                    <tr>
                                        <td valign="top">
                                             <%--Window Dressing - Start--%>
                                            <asp:RadioButtonList ID="rdDateOption" runat="server" CssClass="blackblodtext" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3" >
                                                <asp:ListItem Selected="False"  Text="<%$ Resources:WebResources, Yesterday%>" Value="6"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Today%>"  Value="2"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Tomorrow%>"  Value="7"></asp:ListItem>
                                                <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, Thisweek%>" Value="3"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, ThisMonth%>"  Value="4"></asp:ListItem>
                                                <%--<asp:ListItem Selected="False" Text="Past" Value="0"></asp:ListItem> FB Case 652 Saima --%>
                                                <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Ongoing%>" Value="1"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Custom%>" Value="5"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        <%--Window Dressing - End--%>
                                        </td>
                                    </tr>
                                    <tr id="trDateFromTo" style="display:none" >
                                        <td>
                                            <table>
                                                <tr>
                                                    <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_DateFrom%>" runat="server"></asp:Literal>
                                                        <asp:TextBox ID="txtDateFrom" runat=server Text="" CssClass="altText" onblur="javascript:ChangeEndDate(0)"></asp:TextBox> <%--Edited for FB 1420--%>
                                                        <%--//Code changed by Offshore for FB Issue 1073,1420 -- Start
                                                 <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerFrom" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('txtDateFrom', 'cal_triggerFrom', 0, '%m/%d/%Y');" /> --%>
                                                  <%--ZD 100420--%>
                                                   <a href="#" onclick="document.getElementById('cal_triggerFrom').click();return false;">
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerFrom" style="cursor: pointer;" alt="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>"  title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('txtDateFrom', 'cal_triggerFrom', 0, '<%=format%>');" /><%--ZD 100419--%>
                                                        </a>
                                                 <!--//Code changed by Offshore for FB Issue 1073,1420 -- End-->
						                                <asp:RequiredFieldValidator ID="reqFrom" Enabled="false" ControlToValidate="txtDateFrom" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" ValidationGroup="DateSubmit" runat="server"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_DateTo%>" runat="server"></asp:Literal> &nbsp;&nbsp;&nbsp;<%-- ZD 100369--%>
                                                        <asp:TextBox ID="txtDateTo" runat=server Text="" CssClass="altText" onblur="javascript:ChangeStartDate(0)"></asp:TextBox> <%--Edited for FB 1420--%>
                                                        <%--//Code changed by Offshore for FB Issue 1073,1420 -- Start
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerTo" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('txtDateTo', 'cal_triggerTo', 0, '%m/%d/%Y');" /> --%>
                                                         <%--ZD 100420--%>
                                                            <a href="" onclick="document.getElementById('cal_triggerTo').click();return false;">
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerTo" style="cursor: pointer;" alt="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>"  title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeStartDate(0)" onclick="return showCalendar('txtDateTo', 'cal_triggerTo', 0, '<%=format%>');" /><%--ZD 100419--%>
                                                        </a>
                                                        <!--//Code changed by Offshore for FB Issue 1073,1420 -- End-->
						                                <asp:RequiredFieldValidator ID="reqTo" Enabled="false" ControlToValidate="txtDateTo" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" ValidationGroup="DateSubmit" runat="server"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" rowspan="8" valign="top">
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Locations%>" runat="server"></asp:Literal></td>
                                    </tr>
                                    <tr>
                                        <td align="left" CssClass="blackblodtext">
                                            <%--Window Dressing - Start--%>
                                            <asp:RadioButtonList   ID="rdRoomOption" runat="server" CssClass="blackblodtext" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                                <asp:ListItem Selected="False"  Text="<%$ Resources:WebResources, None%>" Value="0"></asp:ListItem>
                                                <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, Any%>"  Value="1"></asp:ListItem>
                                                <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Selected%>" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        <%--Window Dressing - End--%>
                                        </td>
                                    </tr>
                                    <tr id="trRooms1" style="display:none">
                                        <td>
                                            <table border="0" style="width: 100%">
                                                <tr>
                                                    <td valign="top" align="left" width="80" id="tdCom" runat="server"> <%--Edited for FB 1415,1416,1417,1418--%>
                                                    <input type="button" value="Compare" id="btnCompare" onclick="javascript:compareselected();" class="altShortBlueButtonFormat" runat="server" />
                                                    </td>
                                                    <td valign="top" align="left">
                                                          <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                                              RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                                                              <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, LevelView%>"><span class='blackblodtext'></span></asp:ListItem>
                                                              <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ListView%>"><span class='blackblodtext'>List View</span></asp:ListItem>
                                                          </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>                     
                                        </td>
                                    </tr>
                                    <tr id="trRooms" style="display:none">
                                    
                                    <td  valign="top" align="left">
                <table>
                    <tr>
                        <td align="left" valign="top" style="width:10%">
                         <button id="opnRooms" runat="server"  type="button" class="altMedium0BlueButtonFormat" onclick="javascript:OpenRoomSearch('frmSearchConference');"  ><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, AddRoom%>" runat="server"></asp:Literal></button> <%--ZD 100369--%>
                        <%--<input name="opnRooms" type="button" id="opnRooms" onclick="javascript:OpenRoomSearch('frmSearchConference');" value="Add Room" class="altMedium0BlueButtonFormat" />--%>
                    
                    <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" /><br />
                    <span class="blackblodtext"> <font size="1"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Doubleclickon%>" runat="server"></asp:Literal></font></span>
                        </td>
                        <td align="left" style="width:90%">
                        <select size="4" wrap="false" name="RoomList" id="RoomList" class="treeSelectedNode" onDblClick="javascript:Delroms(this.value)" onkeydown="if(event.keyCode ==32){javascript:Delroms(this.value)}"  style="height:250px;width:100%;" runat="server"></select><%--FB 2694--%> <%--ZD 100420--%>
                        <iframe style="display:none;" name="ifrmLocation" src=""   width="100%" height="300" align="left" valign="top">
                    <p><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_goto%>" runat="server"></asp:Literal> <a id="aLocation" href="" name="aLocation"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_aLocation%>" runat="server"></asp:Literal></a></p>
                  </iframe> 
                        </td>
                    </tr>
                </table>
                  
                </td>
                                        <td style="display:none;"><%--Edited for FB 1415,1416,1417,1418,Panel is Edited && Room Search--%>
                                            <asp:Panel ID="pnlLevelView" runat="server" Height="300px" Width="100%" ScrollBars="Auto" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                                                <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="90%" ShowCheckBoxes="All" onclick="javascript:getRooms(event)"
                                                    ShowLines="True" Width="95%"> 
                                                    <NodeStyle CssClass="treeNode"/>
                                                    <RootNodeStyle CssClass="treeRootNode"/>                                                    
                                                    <ParentNodeStyle CssClass="treeParentNode"/>
                                                    <LeafNodeStyle CssClass="treeLeafNode"/>
                                                </asp:TreeView>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Green">
                                                <%--Added for FB 1415,1416,1417,1418 - Start--%>
                                                <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelect('lstRoomSelection',this);" /><font size="2"> <asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_SelectAll%>" runat="server"></asp:Literal></font>
                                                <br />
                                                <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow"  onclick="javascript:getValues(event)">
                                                </asp:CheckBoxList>
                                                <%--Added for FB 1415,1416,1417,1418  - End--%>
                                            </asp:Panel>
                                             <%--Added for FB 1415,1416,1417,1418  - Start--%>
                                            <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                                                <table><tr align="center"><td>
                                                <asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_YouhavenoRoo%>" runat="server"></asp:Literal>
                                                </td></tr></table>
                                                
                                            </asp:Panel>
                                            <%--Added for FB 1415,1416,1417,1418  - End--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trHost">
                            <td align="left" class="blackblodtext">
                            <%if ((Application["Client"].ToString().ToUpper() == "MOJ")){%> <%--Added For FB 1428--%>
                                Created By
                            <%}else{ %>
                                Host
                              <%} %>
                            </td>
                            <td align="left">
                                <!--[Vivek: 29th Apr 2008]Changed Regular expression added ValidationGroup tag 306-->
                                <asp:TextBox ID="txtHost" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtHost" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters17%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                            </td>
                        </tr>
                        <%if (!(Application["Client"].ToString().ToUpper() == "MOJ")){%> <%--Added For FB 1425--%>
                        <tr id="trParti">
                            <td align="left" class="blackblodtext">
                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Participant%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left">
                                <!--[Vivek: 29th Apr 2008]Changed Regular expression added ValidationGroup tag 306-->
                                <asp:TextBox ID="txtParticipant" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtParticipant" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters17%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator><%--FB 1888--%> 
                            </td>
                        </tr>
                        <%}%><%--Added For FB 1425--%>
                        <%--FB 2694--%>
                        <tr id="trFirstname" style="display:none;">
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_User%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regFN" ControlToValidate="txtFirstName" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters17%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr id="trLastName" style="display:none;">
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_User1%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regLN" ControlToValidate="txtLastName" Display="dynamic" runat="server" ValidationGroup="DateSubmit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters17%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr id="trEmail" style="display:none;">
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_User11%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtEmail" Display="dynamic" runat="server" 
                                    ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Status%>" runat="server"></asp:Literal></td>
                            <td align="left">
                             <%--Window Dressing - Start--%>                            
                                 <asp:RadioButtonList ID="rdStatus" runat="server" CssClass="blackblodtext" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                    <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, Any%>" Value="0"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Pending%>" Value="1"></asp:ListItem>
                                    <asp:ListItem Selected="false" Text="<%$ Resources:WebResources, Nonpending%>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                             <%--Window Dressing - End--%>
                           </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext" style="height: 40px"><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Access%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdPublic" runat="server" CssClass="blackblodtext" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                    <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, Any%>" Value="0"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Public%>"  Value="1"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Private%>"  Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        </table>                       
                        
                        <%--FB 2632 Starts--%>
                        <%--FB 2670 Starts--%>
                        <tr id="trConcierge" runat="server">
                            <td align="left" colspan="2">
                            <asp:Label ID="Label4" runat="server" CssClass="subtitleblueblodtext"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_ConfSupportSearch%>" runat="server"></asp:Literal></asp:Label><%--FB 3023--%>
                                <table id="tblConciergeNew"  cellspacing="2" cellpadding="3" border="0" style="width:40%;  margin-left:20px">
                                    <tr id="tdandor" runat="server" valign="top" align="center" >
                                           <td  align="right" nowrap="nowrap"> <%--FB 2670--%>
                                                <asp:RadioButton ID="radAnd" runat="server" GroupName="ConSupport"  />
                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_AND%>" runat="server"></asp:Literal></span>
                                          </td>
                                          <td align="left" nowrap=nowrap valign="baseline"> <%--FB 2670--%>
                                                <asp:RadioButton ID="radOr" runat="server" GroupName="ConSupport" Checked="true"/> <%--ZD 103445--%>
                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_OR%>" runat="server"></asp:Literal></span>
                                          </td>
                                          <%--FB 2729 Starts--%>
                                           <td  align="left" nowrap="nowrap"> 
                                                <asp:RadioButton ID="radPending" runat="server" GroupName="VNOCStatus"  />
                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Pending%>" runat="server"></asp:Literal></span>
                                                <asp:RadioButton ID="radAssigned" runat="server" GroupName="VNOCStatus" />
                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_Assigned%>" runat="server"></asp:Literal></span>
                                                 <asp:RadioButton ID="radAll" runat="server" GroupName="VNOCStatus" Checked="true" /> <%--ZD 103445--%>
                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_All%>" runat="server"></asp:Literal></span>
                                          </td>
                                          <%--FB 2729 Ends--%>
                                    </tr>
                                    <tr id="trOnSiteAVSupport" runat="server">
                                    <td valign="top" colspan="2">
                                        <table style="border:collapse;width:100%;">
                                         <tr valign="top">
                                             <td id="tdonSiteAV" runat="server"  valign="top" align="left"  nowrap="nowrap" style="width: 20%;">
                                                <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                                <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_OnSiteAVSuppo%>" runat="server"></asp:Literal></span>
                                             </td>
                                          </tr>
                                          <tr>
                                        <td valign="top" id="tdMeetandGreet" runat="server" align="left" nowrap="nowrap" style="width: 2%;">
                                               <input id="chkMeetandGreet" type="checkbox" runat="server" />
                                               <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_MeetandGreet%>" runat="server"></asp:Literal></span>
                                        </td>
                                       </tr>
                                       <tr></tr>
                                       <tr></tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                    <table style="border:collapse;width:100%;" border="0">
                                         <tr>
                                         <td id="tdConciergeMonitoring" runat="server" align="left" valign="top" nowrap="nowrap" style="width: 100%;">
                                            <input id="chkConciergeMonitoring" type="checkbox" runat="server" />
                                            <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_CallMonitoring%>" runat="server"></asp:Literal></span> <%--FB 3023--%>
                                           </td>
                                         </tr>
                                         <tr>
                                             <td id="tdDedicatedVNOC" runat="server" align="left" nowrap=nowrap valign="top" colspan="2">
                                               <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server"  />
                                               <span class='blackblodtext'><asp:Literal Text="<%$ Resources:WebResources, SearchConferenceInputParameters_DedicatedVNOC%>" runat="server"></asp:Literal></span>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td nowrap="nowrap">
                                             <div style="white-space:nowrap; display:inline">
                                             <asp:TextBox ID="txtVNOCOperator" TextMode="MultiLine" runat="server" CssClass="altText"></asp:TextBox>
                                                <a href="javascript:getVNOCEmailList()" onmouseover="window.status='';return true;" runat="server" id="addclick">
                                             <img id="imgVNOC" runat="server" src="../image/VNOCedit.gif" alt="<%$ Resources:WebResources, Edit%>" runat="server" border="0" style="border: 0; cursor:pointer" title="<%$ Resources:WebResources, SelectVNOCOperator%>" /></a><%--FB 2670 FB 2783--%> <%--FB 2798--%><%--ZD 100419--%>
                                             <a href="javascript: deleteVNOC();" onmouseover="window.status='';return true;">
                                             <img border="0" runat="server" id="imgdeleteVNOC" src="image/deleteall.gif" alt="<%$ Resources:WebResources, RemoveAll%>" title="<%$ Resources:WebResources, RemoveAll%>" width="16" height="16" runat="server" /></a> <%--FB 2798--%> <%--ZD 100369--%>
                                             </div>
                                            </td>
                                            <td><asp:TextBox ID="hdnVNOCOperator" runat="server" style="display:none;width:0px"></asp:TextBox></td>
                                           </tr>
                                        </table>
                                      </td>
                                   </tr>
                               </table>
                               
                        <%--FB 2670 Ends--%>
                        <%--FB 2632 Starts--%>
                        </td>
                        </tr>
                         <tr>
                         <%--Custom Attributes--%>
                            <td align="left" colspan="2">
                           <asp:Label ID="Label3" runat="server" CssClass="subtitleblueblodtext"><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_CustomOptSearch%>" runat="server"></asp:Literal></asp:Label> <%--Edited for FB 1428 MOJ Changes--%>
                           <div style="margin-left:20px">
                                <asp:Table runat="server" ID="tblCustomAttribute" Width="80%" CellPadding="3" cellspacing="2" Visible="true">
                                </asp:Table>
                                </div>
                            </td>
                        </tr>
                        <%--FB 2377 FB 2632--%>                    
                </td>
            </tr>
            <tr runat="server" id="trSaveSearch" style="display:none">
                <td align="left">
                <div>
                    <asp:Label ID="lblSearch" runat="server" CssClass="subtitleblueblodtext" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_lblSearch%>"></asp:Label>
                    <asp:TextBox ID="txtSearchTemplateName" ValidationGroup="TemplateSubmit" runat="server" MaxLength="26" CssClass="altText" Text=""></asp:TextBox><%--FB 1953--%>
                    <asp:TextBox ID="txtSearchTemplateID" runat="server" CssClass="altText" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqName" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ControlToValidate="txtSearchTemplateName" ValidationGroup="TemplateSubmit"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtSearchTemplateName" Display="dynamic" runat="server" ValidationGroup="TemplateSubmit" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters18%>" ValidationExpression="[A-Za-z0-9._~?!`* \-]+"></asp:RegularExpressionValidator> <%--fogbugz case 280--%>
                </div>
                </td>
            </tr>
            <tr>
                <td>
                <%--Window Dressing--%>
                    <table cellpadding="2" cellspacing="2" border="0" align="center">
                        <tr>
                            <td>
                                <%--ZD 100369--%>
                                <button id="btnReset" onserverclick="Reset" class="altMedium0BlueButtonFormat" runat="server"><asp:Literal Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button>
                                <%--<asp:Button id="btnReset" CssClass="altMedium0BlueButtonFormat" Text="Reset" OnClick="Reset" runat="server" />--%>
                            </td>
                            <td>
                                <asp:Button id="btnSaveSearch" CssClass="altMedium0BlueButtonFormat" style="width:180px;" OnClick="SaveSearch" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_btnSaveSearch%>" OnClientClick="javascript:return SaveSearch('2');" runat="server" ValidationGroup="TemplateSubmit" />
                            </td>
                            <td>
                                <asp:Button id="btnSubmit" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, SearchConferenceInputParameters_btnSubmit%>" OnClick="SubmitSearch" runat="server" OnClientClick="javascript:return SubSearch();" ValidationGroup="DateSubmit" /><%--Edited for FB 1420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			<%--ZD 101443 Starts--%>
            <tr>
                <td align="left" style="margin-left:20px">
                    <table id="tblSearchUser" runat="server" width="100%">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblUR" runat="server" CssClass="subtitleblueblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, Search_UserSearch%>" runat="server"></asp:Literal>
                                </asp:Label>        
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="blackblodtext" > <asp:Literal Text="<%$ Resources:WebResources, UserName%>" runat="server" ></asp:Literal></span>
                            
                                <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="False" Style="width: 15%"></asp:TextBox>
                                &nbsp;  
                                <a id="ImgApprover1" href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;">
                                    <img id="Img1" border="0" src="image/edit.gif" alt="Edit" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" />
                                </a>
                                <a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                    <img id="Img2" border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>" />
                                </a> 
                                <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="txtApprover1"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="<%$ Resources:WebResources, Required%>"
                                                ValidationGroup="SubmitUserSearch"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="hdnApprover1" runat="server" Width="0px" BackColor="White" BorderColor="White" BorderStyle="None" Style="display: none"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList ID="UserSearchList"  CssClass="blackblodtext" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3" CellPadding="3" CellSpacing="3" runat="server" >
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Conferences%>" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, Rooms%>"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="<%$ Resources:WebResources, DefaultLicense_MCUs%>"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="<%$ Resources:WebResources, AV%>"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="<%$ Resources:WebResources, Menu%>"></asp:ListItem>
                                    <asp:ListItem Value="6" Text="<%$ Resources:WebResources, Facilities%>"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="<%$ Resources:WebResources, AVWorkorder%>"></asp:ListItem>
                                    <asp:ListItem Value="8" Text="<%$ Resources:WebResources, MenuWorkorder%>"></asp:ListItem>
                                    <asp:ListItem Value="9" Text="<%$ Resources:WebResources, FacilityWorkorder%>"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <button type="button" id="btnUserSearch" runat="server" class="altLongYellowButtonFormat" onserverclick="OpenUserSearch" 
                                 validationgroup="SubmitUserSearch" style="width:100px">
									<asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal>
                                </button> 
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
			<%--ZD 101443 End--%>
        </table>
</center>
                <input type="hidden" id="helpPage" value="81" tabindex="-1" /> 

    </form>
    
    <%--ZD 100369 Starts--%>
    <div id="PopupMCUList" class="rounded-corners" style="position: absolute;
    overflow:hidden; border: 0px;
    width: 1000px; display: none;">    
    <iframe src="" id="MCUList" name="MCUList" style="height: 530px; border: 0px; overflow:hidden; width: 1000px; overflow: hidden;"></iframe>
	</div>
	<%--ZD 100369 End--%>
    
    <script language="javascript">
    //alert(document.getElementById("rdRoomOption_2").checked);
    if (document.getElementById("rdRoomOption_2").checked)
        changeRoomSelection("2");
    else
        changeRoomSelection("0");

    if (document.getElementById('btnSaveSearch').style.display != 'none') {
        document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnSaveSearch').setAttribute('onfocus', '');document.getElementById('btnSaveSearch').focus();");
        document.getElementById('btnSaveSearch').setAttribute("onblur", "document.getElementById('btnSubmit').setAttribute('onfocus', ''); document.getElementById('btnSubmit').focus()");
    }
    //ZD 100369 start
    if (document.getElementById("rdMCUOption_1").checked)
        changeMCUSelection("1");
    else
        changeMCUSelection("0");
    //ZD 100369 End
    
//if (document.getElementById("trSaveSearch").style.display == "")
//	SaveSearch('0');
//Added FB 1420 -- Start
function SubSearch() //FB 2670
{
    if (document.getElementById('hdnSelEntityCodeVal') != null)
    {
        if (document.getElementById('hdnSelEntityCodeVal').value != "" ) {
            if (document.getElementById('hdnSelEntityCodeVal').value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/) == -1) {
                document.getElementById('hdnEntityRegID').style.display = 'block';
                return false;
            }
        }   
    }
    //FB 2632 - Starts
    if (document.getElementById("chkDedicatedVNOCOperator").checked) {
        if (document.getElementById("hdnVNOCOperator").value == "") {
            alert(SelectVNOC);
            return false;
        }
        
    }
    //FB 2632 - End

    //ZD 100369-MCUFailOver - Starts
    if (document.getElementById("rdMCUOption_1").checked) {
        if (document.getElementById("txtSelectedMCU").value == "") {
            alert(SelectMCU);
            return false;
        }

    }
    //ZD 100369-MCUFailOver - Starts
    
    if(document.getElementById("trSaveSearch").style.display != "none")
    {
        if(document.getElementById("txtSearchTemplateName").value == "")
        {
            alert(SearchNameValidation);
        return false;
        }
        return true;
    }
    if(document.getElementById("rdRoomOption_2").value == "2")
    {
        if(document.getElementById("trRooms1").style.display != "none")
        {
            if(document.getElementById("rdSelView").disabled == false)
            {
                if(document.getElementById("selectedloc").value == "")
                {
                    alert(SelectRooms);
                    return false;
                }
            }
         }
    }
    if(document.getElementById("trDateFromTo").style.display != "none")
    {
        if(document.getElementById("txtDateFrom").value == "" || document.getElementById("txtDateTo").value == "")
        {
            alert(SelectCustomDate);
            return false;
        }
        return true;
    }
}
//Added for FB 1420 -- End


$(document).ready(function() {
$('#btnSelectMCU').click(function() {
        $('#popupdiv').fadeIn();
        $("#MCUList").attr("src", "BridgeList.aspx");
        $('#PopupMCUList').show();
        $('#PopupMCUList').bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
    });
});



//FB 2870 Start
if ('<%=Session["EnableNumericID"]%>' == '1') {
    document.getElementById("trCTSNum").style.visibility = 'visible';
    document.getElementById("txtNumericID").style.visibility = 'visible';
    document.getElementById("btnNumericID").style.visibility = 'visible';
}
else {
    document.getElementById("trCTSNum").style.visibility = 'hidden';
    document.getElementById("txtNumericID").style.visibility = 'hidden';
    document.getElementById("btnNumericID").style.visibility = 'hidden';

}
//FB 2870 End

//ZD 101433
function deleteApprover(id) {
    eval("document.getElementById('hdnApprover" + (id + 1) + "')").value = "";
    eval("document.getElementById('txtApprover" + (id + 1) + "')").value = "";
}

//ZD 101525
if ('<%=Session["admin"]%>' != '2') {
    document.getElementById("tblSearchUser").style.display = "none";
}


</script>
</body>
</html>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2968 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2968 Ends -->--%>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
