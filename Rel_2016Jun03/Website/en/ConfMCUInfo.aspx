<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_ConfMCUInfo.ConfMCUInfo" %><%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ConfMCUInfo</title>
    <script type="text/javascript">    // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

    <script type="text/javascript" src="script/errorList.js"></script>
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

</head>
<body>
    <form id="frmconfMCUinfo" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
        <center>
            <br />
            <h3>
                <asp:Label ID="lblMCUInfo" runat="server" Font-Bold="true" Text="<%$ Resources:WebResources, ConfMCUInfo_lblMCUInfo%>"></asp:Label></h3>
            <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table border="0" width="70%" align="center" cellpadding="2" cellspacing="3">
            <tr>
                <td class="subtitlexxsblueblodtext" colspan="3"><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_ConferenceDeta%>" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td width="10%">
                </td>
                <td align="left" nowrap="nowrap" width="25%" valign="top"><%--FB 2508--%>
                    <b><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_Title%>" runat="server"></asp:Literal></b>
                </td>
                <td width="65%">
                    <asp:Label ID="lblConfName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
                <td align="left" nowrap="nowrap">
                    <b><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_StartDatetime%>" runat="server"></asp:Literal></b>
                </td>
                <td>
                    <asp:Label ID="lblStartTime" runat="server"></asp:Label>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="subtitlexxsblueblodtext" nowrap="nowrap" colspan="3"><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_ServerTime%>" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="left" nowrap="nowrap">
                    <b><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_Web%>" runat="server"></asp:Literal></b>
                </td>
                <td>
                    <asp:Label ID="lblWebServerTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trDatabaseServerTime" runat="server">
                <td>
                </td>
                <td align="left" nowrap="nowrap">
                    <b><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_DataBase%>" runat="server"></asp:Literal></b>
                    <br />
                </td>
                <td>
                    <asp:Label ID="lblDatabaseServerTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="subtitlexxsblueblodtext" colspan="3"><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_MCU%>" runat="server"></asp:Literal></td>
            </tr>
            <tr class="blackblodtext">
                <td align="center" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_Name%>" runat="server"></asp:Literal></td>
                <td align="center"><asp:Literal Text="<%$ Resources:WebResources, ConfMCUInfo_LocalizedTime%>" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td height="1" colspan="3" bgcolor="CCCCCC">
                </td>
            </tr>
            <tr align="center">
                <td colspan="3">
                    <table id="tblConfMCUinfo" runat="server" border="0" width="90%">
                    </table>
                    <asp:Label runat="server" id="tdNoMCU" visible="false" text="<%$ Resources:WebResources, ConfMCUInfo_tdNoMCU%>"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
<%--ZD 100428 END--%>
