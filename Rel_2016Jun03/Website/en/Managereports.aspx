<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.	
*
* You should have received a copy of the RMT license with	
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#"  %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNet.aspx" -->
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }    
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Manage Reports</title>
</head>
<body>
   
    <form  id="frmManagereport" name="frmManagereport">
        <div>
                <table border="0" cellpadding="4" cellspacing="6" width="98%">
                <tr>
                    <td colspan="3">
                         <iframe id="RoomFrame"  width="100%" valign="top" height="625px" scrolling="auto" src="../Newreports/ReportGraphs.aspx" ></iframe>
                    </td>
                </tr>
                    
                </table>
            



        </div>
    </form>
</body>
</html>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

