/*ZD 100147 Start*/
/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/
/*ZD 100147 ZD 100899 End*/
function ViewWorkorderDetails(woID, confID)
{
  	if (woID != "") {
		url = "ViewWorkorderDetails.aspx?woID=" + woID + "&confID=" + confID;	
		//document.location.href = url;//ZD 101314
		myPopup = window.open(url,'wordorder','status=no,width=850,height=490,resizable=yes,scrollbars=yes');
		myPopup.focus();
		if (!myPopup.opener) {
			myPopup.opener = self;
		}

	}
}

function ViewWorkorderDetails(woID, confID, ttype)
{
  	if (woID != "") {
		url = "ViewWorkorderDetails.aspx?woID=" + woID + "&confID=" + confID + "&t=" + ttype+ "&hf="	//COde added for WO bug
		//document.location.href = url;//ZD 101314        
		myPopup = window.open(url, 'WorkOrder','status=no,width=850,height=490,resizable=yes,scrollbars=yes');
		myPopup.focus();
		if (!myPopup.opener) {
			myPopup.opener = self;
		}
	}
}

//Added for WO bug
function ViewWorkorderDetailspopup(woID, confID, ttype, ispopup)
{
  	if (woID != "") {
		url = "ViewWorkorderDetails.aspx?woID=" + woID + "&confID=" + confID + "&t=" + ttype+ "&hf=1" ;	//COde added for WO bug
		//document.location.href = url;//ZD 101314
		
	}
}