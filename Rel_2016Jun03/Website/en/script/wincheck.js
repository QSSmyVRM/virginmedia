/*ZD 100147 Start*/
/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/
/*ZD 100147 ZD 100899 End*/
// FB 3055
var url1 = window.parent.location.href;
var url2 = window.location.href;
var url3 = window.opener;
var cnt = 0;
if (url3 == undefined || url3 == null)
    cnt++;
if (url1 == url2)
    cnt++;
if (cnt == 2)
    window.location.href = "thankyou.aspx";
/*
var url = window.opener;
if (url == undefined || url == null)
    window.location.href = "genlogin.aspx";
*/