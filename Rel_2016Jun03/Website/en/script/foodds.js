/*ZD 100147 Start*/
/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/
/*ZD 100147 ZD 100886 End*/
// script for food data structure

function FoodItem(fid, name, imgSrc)
{
	this.fid = fid;
	this.name = name;
	this.icon = new Image();
	this.icon.src = imgSrc;
	this.ddobj = null;
	this.toString = foodToString;
}

function foodToString()
{
	return this.name;
}

function compareName(a, b)
{
	var aname = a.name.toUpperCase();
	var bname = b.name.toUpperCase();
	if (aname > bname) 
		return 1;
	else if (aname < bname)
		return -1;
	else 
		return 0;
}


