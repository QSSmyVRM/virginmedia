/*ZD 100147 Start*/
/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/
/*ZD 100147 ZD 100886 End*/
if (navigator.appName == "Microsoft Internet Explorer") {
	popwintester = window.open("expired_page.htm",'','status=no,width=0,height=0,left=5000,top=5000,resizable=no,scrollbars=no');
	if (popwintester) {
		popwintester.close();
	} else {
		alert("Your browser has a popup blocker enabled. Please disable it for the RMT website, as RMT uses popups that are necessary for your information input.");
		popwintester = null;
	}
}
