/*ZD 100147 Start*/
/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/
/*ZD 100147 ZD 100886 End*/
var RoomDisplayMod = 1;

function chgRoomDisplay(locpghref, mod)
{
	mod = parseInt(mod);
	if (mod != RoomDisplayMod) {
		RoomDisplayMod = mod;
		ifrmLocation.location.href = locpghref + "&mod=" + mod
	}
}
