<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<script type="text/javascript">    // FB 2815
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); 
</script>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0"
    marginwidth="0">
    <%--ZD 100040 - Start--%>
    <form id="frmMCUMember" method="POST" runat="server">    
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		<Scripts>                
			<asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		</Scripts>
	</asp:ScriptManager>
    <%--ZD 100040 - End--%>
    <script language="JavaScript" src="inc/functions.js"></script>
    <script type="text/javascript" src="script/wincheck.js"></script>
    <script runat="server">

        protected override void InitializeCulture()
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    </script>
    <!-- JavaScript begin -->
    <script language="JavaScript">
<!--

        var oldpartyno = 0;
        var newpartyno = 0;

        function assemble(ary, endstr) {
            var newstr = "";
            for (var i = 0; i < ary.length - 1; i++) {
                newstr += ary[i] + endstr;
            }
            newstr += ary[ary.length - 1];
            return newstr;
        }

        function AddSelection(id, varSel, ctrlName) {
            
            if (parent.document.frmManageMCULBgroup.MCUsInfo) 
            {
                mcusinfo = parent.document.frmManageMCULBgroup.MCUsInfo.value;
                var tmpMCUInfo = "";                
                mcusary = mcusinfo.split("||");
                for (var i = 0; i < mcusary.length - 1; i++) {
                    if (mcusary[i] == "")
                        continue;
                    var mcus1ary = mcusary[i].split("!!");

                    if (id == mcus1ary[0]) 
                    {
                        if (varSel == "LB") {
                            if (ctrlName.checked == true)
                                mcus1ary[4] = "1";
                            else
                                mcus1ary[4] = "0";
                        }
                        else if (varSel == "OF") {
                            if (ctrlName.checked == true)
                                mcus1ary[5] = "1";
                            else
                                mcus1ary[5] = "0";
                        }
                        else if (varSel == "LM") {
                            if (ctrlName.checked == true)
                                mcus1ary[6] = "1";
                            else
                                mcus1ary[6] = "0";
                        }

                        if (tmpMCUInfo == "")
                            tmpMCUInfo = mcus1ary[0] + "!!" + mcus1ary[1] + "!!" + mcus1ary[2] + "!!" + mcus1ary[3] + "!!" + mcus1ary[4] + "!!" + mcus1ary[5] + "!!" + mcus1ary[6];
                        else
                            tmpMCUInfo += "||" + mcus1ary[0] + "!!" + mcus1ary[1] + "!!" + mcus1ary[2] + "!!" + mcus1ary[3] + "!!" + mcus1ary[4] + "!!" + mcus1ary[5] + "!!" + mcus1ary[6];
                    }
                    else 
                    {
                        if (tmpMCUInfo == "")
                            tmpMCUInfo = mcusary[i];
                        else
                            tmpMCUInfo += "||" + mcusary[i];
                    }
                }

                parent.document.frmManageMCULBgroup.MCUsInfo.value = tmpMCUInfo + "||";
            }
        }

        function deleteThisParty(thisparty) {
            var needRemove = confirm(RemoveMCUs); //ZD 100040
            if (needRemove == true) 
            {
                if (thisparty != "") 
                {
                    mcusinfo = parent.document.frmManageMCULBgroup.MCUsInfo.value;

                    if ((tmploc = mcusinfo.indexOf("!!" + thisparty + "!!")) != -1)
                        parent.document.frmManageMCULBgroup.MCUsInfo.value = mcusinfo.substring(0, mcusinfo.lastIndexOf("||", tmploc) + 1) + mcusinfo.substring(mcusinfo.indexOf("||", tmploc) + 1, mcusinfo.length); ;

                    parent.refreshIframe();
                }
            }
        }

        function frmManagemculist_Validator() {
            return true;
        }

//-->
    </script>
    <!-- JavaScript finish -->
    <!-- JavaScript begin -->
    <script language="JavaScript">
<!--
        var oldpartyno = 0;
        var newpartyno = 0;
        
        hdnMCUSel = parent.document.frmManageMCULBgroup.hdnMCUSelected.value;
        if (parent.document.frmManageMCULBgroup.MCUsInfo) {
            mcusinfo = parent.document.frmManageMCULBgroup.MCUsInfo.value;
        } else {
            mcusinfo = ""
            setTimeout('window.location.reload();', 500);
        }
        //mcusinfo = "2!!veni!!admin@RMT.com!!Eastern!!1||3!!veninew!!adminnew@RMT.com!!Eastern!!1||";
        _d = document;
        var mt = "";

        mt += "<div align='center'>";
        mt += "  <form name='frmManagemculist' method='POST' action='dispatcher/userdispatcher.asp' onsubmit='return frmManagemculist_Validator()' language='JavaScript'>"
        mt += "  <table border='0' cellpadding='3' cellspacing='1' width='100%'>";

        mcusary = mcusinfo.split("||");
        for (var i = 0; i < mcusary.length - 1; i++) {
            tdbgcolor = (i % 2 == 0) ? "" : "#E0E0E0";
            mcuary = mcusary[i].split("!!");

            isLoadBal = "checked";
            isOverFlow = "checked";
            isLeadMCU = "checked";
            if (mcuary[4] == "0") isLoadBal = "";
            if (mcuary[5] == "0") isOverFlow = "";
            if (mcuary[6] == "0") isLeadMCU = "";
            
            mt += "    <tr class='tableBody'>"
            mt += "      <td align='center' class='tableBody' width='3%' >" + " <a href='#' onCLick='Javascript:deleteThisParty(\"" + mcuary[1] + "\")'><img border='0' src='image/btn_delete.gif' alt='Delete' width='18' height='18' style='cursor:pointer;' title='<asp:Literal Text='<%$ Resources:WebResources, Delete%>' runat='server'></asp:Literal>'></a></td>"
            mt += "      <td align='center' class='tableBody' width='20%'>" + mcuary[1] + "</td>"
            mt += "      <td align='center' class='tableBody' width='20%'>" + mcuary[2] + "</td>"
            mt += "      <td align='center' class='tableBody' width='27%'>" + mcuary[3] + "</td>"
            mt += "      <td align='center' class='tableBody' width='10%' style='display:none;'> <input  type='checkbox' disabled='disabled' checked='checked' " + isLoadBal + " onCLick='Javascript:AddSelection(\"" + mcuary[0] + "\",\"LB\",this)' name='loadBal' id='loadBal" + i + "'></td>"
            mt += "      <td align='center' class='tableBody' width='10%' style='display:none;'> <input type='checkbox' " + isOverFlow + " onCLick='Javascript:AddSelection(\"" + mcuary[0] + "\",\"OF\",this)' name='overflow'  id='overflow" + i + "'> </td>"
            mt += "      <td align='center' class='tableBody' width='10%'><input type='radio'" + isLeadMCU + " onCLick='Javascript:AddSelection(\"" + mcuary[0] + "\",\"LM\",this)' name='LeadMCUGroup' id='leadmcu" + i + "'/></td>"
            mt += "    </tr>"
        }

        mt += "  </table>"
        mt += "  </form>"

        _d.write(mt)

        function replaceSpcChar(strName) {
            do {
                strName = strName.replace("?", "\"");
            } while ((strName.indexOf('?')) >= "0");

            return strName;
        }

//-->
    </script>
    </form><%--ZD 100040--%>
</body>
</html> 