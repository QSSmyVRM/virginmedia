<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.
*
* You should have received a copy of the RMT license with
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%>
<%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.AddTerminalEndpoint" %>

<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2779 -->

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<% if(Request.QueryString["ifrm"] == null){%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<% }%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create/Edit Endpoint</title>
    <script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function viewMCU(val)
{
	//url = "dispatcher/admindispatcher.asp?cmd=ViewBridge&bid=" + val.split("@")[0];
    var mcuid =  val.split("@")[0];
    
    if(mcuid != "-1" && mcuid != "")
    {
        url = "BridgeDetailsViewOnly.aspx?hf=1&bid="+ mcuid;
        window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    }
    return false;
}

    function CheckIPSelection(obj)
    {
        if (obj.tagName == "INPUT" && obj.type == "radio")
        {
            for (i=0; i<document.frmTerminalControl.elements.length;i++)
            {
                var obj1 = document.frmTerminalControl.elements[i];
                if (obj1.tagName == "INPUT" && obj1.type == "radio")
                    if (obj1.name != obj.name)
                        obj1.checked = false;
//                    else
//                    {
//                        var temp = obj.parentElement.parentElement.innerHTML;
//                        alert(temp);
//                    }
            }
        }
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
        
    </script>
</head>
<body>
    <form id="frmTerminalControl" runat="server" method="post">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <center><table width="98%" border="0" cellpadding="2" cellspacing="2" >
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="<%$ Resources:WebResources, AddTerminalEndpoint_lblHeader%>"></asp:Label></h3><br />
                     <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                     <%--ZD 100678 start--%>
                      <div id="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                      </div><%--ZD 100176--%><%--ZD 100678 End--%>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="20" class="tableHeader">1</td>
                            <td class="subtitleblueblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_BasicInformati%>" runat="server"></asp:Literal></td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table width="90%" style="margin-left:50px">
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" width="20%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_EndpointName%>" runat="server"></asp:Literal><span class="reqfldText">*</span>
                            </td>
                            <td align="left" width="35%">
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqEndpointName" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="<%$ Resources:WebResources, Required%>"  ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <asp:TextBox CssClass="altText"  ID="txtEndpointID" runat="server" Visible="false"></asp:TextBox>
                            </td>
                            <%--Window Dressing--%>
                            <td align="left" width="24%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_TerminalType%>" runat="server"></asp:Literal></td>
                            <td align="left" width="28%">&nbsp<asp:Label ID="lblTerminalType" CssClass="subtitleblueblodtext" runat="server" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Table ID="tblEndpointName" runat="server" Width="100%" CellPadding="0" CellSpacing="0" >
                                    <asp:TableRow VerticalAlign="Top">
                                    <%--Window Dressing--%>
                                        <asp:TableCell Width="20%" HorizontalAlign="left" CssClass="blackblodtext"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, EndpointLastName%>" runat="server"></asp:Literal></asp:TableCell>
                                        <asp:TableCell Width="35%" HorizontalAlign="left">
                                            <asp:TextBox ID="txtEndpointLastName" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                             <%--Code added for FB : 1175 Start--%>
                                             <%--<asp:RequiredFieldValidator ID="reqtxtEndpointLastName" ControlToValidate="txtEndpointLastName" ValidationGroup="Submit" runat="server" ErrorMessage="  Required" ></asp:RequiredFieldValidator>--%><%--FB 2528--%>
                                             <%--Code added for FB : 1175 End--%>
                                        </asp:TableCell>
                                    <%--Window Dressing--%>
                                        <asp:TableCell Width="24%" HorizontalAlign="left"  class="blackblodtext" ><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, EndpointLastName%>" runat="server"></asp:Literal><span class="reqfldText">*</span>
                                           </asp:TableCell>
                                          <asp:TableCell Width="25%">
                                            <asp:TextBox ID="txtEndpointEmail" runat="server" CssClass="altText" Text="" ></asp:TextBox>
                                             <%--Code added for FB : 1175 Start--%>
                                            <asp:RequiredFieldValidator ID="reqtxtEndpointEmail" ControlToValidate="txtEndpointEmail" ValidationGroup="Submit" runat="server" ErrorMessage="  <%$ Resources:WebResources, Required%>" ></asp:RequiredFieldValidator>
                                             <%--Code added for FB : 1175 End--%>
                                        </asp:TableCell>                                        
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2">
                                        </asp:TableCell>
                                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                         <%--Code added for FB : 1640 --%>
                                            <asp:RegularExpressionValidator ID="regEndPtEmail" ControlToValidate="txtEndpointEmail"  ValidationGroup="Submit" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters23%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:#$,%&'~]*$"></asp:RegularExpressionValidator>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="20" class="tableHeader">2</td>
                            <td class="subtitleblueblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_EndpointParame%>" runat="server"></asp:Literal></td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" style="margin-left:50px">
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" width="18%" >
                                <asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_Protocol%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left" width=31%>
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqlstProtocol" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ControlToValidate="lstProtocol" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </td>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext" width="22%"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_ConnectionType%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" CssClass="altText"></asp:DropDownList> <%--Fogbugz case 427--%>
                                <%--Code added for FB : 1175 Start--%>
                                <asp:RequiredFieldValidator ID="reqConnectionType" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ControlToValidate="lstConnectionType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <%--Code added for FB : 1175 End--%>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_AddressType%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                 <%--Code added for FB : 1175 Start--%>
                                <asp:RequiredFieldValidator ID="reqAddressType" runat="server" InitialValue="-1" ControlToValidate="lstAddressType" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                 <%--Code added for FB : 1175 End--%>
                                </td>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_Address%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtAddress" runat="server" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" ></asp:RequiredFieldValidator>
                                <%--FB 1972--%>
                                <asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters24%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|=`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                            </td>
                        </tr>
                        <%--FB 2365 Start--%>
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_ConferenceCode%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtconfcode" runat="server" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regconfcode" ControlToValidate="txtconfcode" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters24%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|=`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator> 
                            </td>
                            <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_LeaderPin%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtLeaderpin" runat="server" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regleaderpin" ControlToValidate="txtLeaderpin" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters24%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|=`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--FB 2365 End--%>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_Model%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="reqVideoEquipment" runat="server" InitialValue="-1" ControlToValidate="lstVideoEquipment" ValidationGroup="Submit" ErrorMessage="Required"></asp:RequiredFieldValidator> --%> <%--FB 3046 -AS Conferencesetup is not validating--%>
                                </td>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_PreferredBandw%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left">
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqLineRate" runat="server" InitialValue="-1" ControlToValidate="lstLineRate" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>                                    
                                </td>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_WebAccessURL%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtURL" runat="server" TextMode="SingleLine"></asp:TextBox>
                            </td>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_LocatedOutside%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:CheckBox ID="chkIsOutside" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                             <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_Connection%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left">
                                <%--Code added for FB : 1475 End
                                <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstConnection"   DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                --%>
                                <asp:DropDownList CssClass="altLong0SelectFormat" runat="server" ID="lstConnection">
                                    <asp:ListItem Text="<%$ Resources:WebResources, PleaseSelect%>" Value="-1"></asp:ListItem> 
                                    <asp:ListItem Text="<%$ Resources:WebResources, AudioOnly%>" Value="1"></asp:ListItem>  <%--FB 1744 --%>
                                    <asp:ListItem Text="<%$ Resources:WebResources, AudioVideo%>" Value="2"></asp:ListItem> <%--FB 1744 --%>
                                </asp:DropDownList>
                                 <%--Code added for FB : 1475 End --%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="-1" ControlToValidate="lstConnection" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>                                    
                                </td>
                                    <%--Window Dressing--%>
                           <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_EncryptionPref%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:CheckBox ID="chkEncryptionPreferred" runat="server" />
                            </td>
                        </tr>
                        <tr>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_EmailID%>" runat="server"></asp:Literal></td> <%-- ICAL Cisco Telepresence fix--%>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtExchangeID" runat="server" Width="215px" TextMode="SingleLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtExchangeID" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters22%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <%--Api Port Starts--%>
                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_APIPort%>" runat="server"></asp:Literal></td>
                        <td align="left">
                        <asp:TextBox CssClass="altText"  ID="txtapiportno" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtapiportno" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </td>
                        <%--Api Port Ends--%>
                        </tr>
                        <%--ZD 101446 Starts--%>
                        <tr>
                        <td align="left" class="blackblodtext"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ParticipantCode%>" runat="server"></asp:Literal></td> <%-- ICAL Cisco Telepresence fix--%>
                            <td align="left">
                                <asp:TextBox CssClass="altText"  ID="txtPartyCode" runat="server" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtPartyCode" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters24%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|=`\[\]{}\=^$%&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        <%--Api Port Ends--%>
                        </tr>
                        <%--ZD 101446 Ends--%>
                    </table>
                </td>
             </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="20" class="tableHeader">3</td>
                            <td class="subtitleblueblodtext" align="left"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_MCUParameters%>" runat="server"></asp:Literal></td>                            
                        </tr>
                    </table>
                </td>
            </tr>
             <tr>
                <td>
                    <table width="100%" style="margin-left:50px">
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="left" width="18%" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_AssignedtoMCU%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left" colspan="3" >
                                <asp:DropDownList CssClass="altLong0SelectFormat" OnSelectedIndexChanged="DisplayBridgeDetails" AutoPostBack="true" ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList>&nbsp;&nbsp;
                                <input type="button" name="btnViewMCU" runat="server" value="<%$ Resources:WebResources, AddNewEndpoint_btnViewMCU%>" class="altMedium0BlueButtonFormat" onClick="javascript: viewMCU(document.frmTerminalControl.lstBridges.options[document.frmTerminalControl.lstBridges.selectedIndex].value);">
                                <asp:RequiredFieldValidator ID="reqBridges" runat="server" InitialValue="-1" ControlToValidate="lstBridges" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>                                    
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center" >
                                <h5><font class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_IPServices%>" runat="server"></asp:Literal></font></h5> <%-- Organization Css Module --%>
                                 <asp:DataGrid runat="server" EnableViewState="true" ID="dgIPServices" AutoGenerateColumns="false"
                                      CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="90%">
                                   <%--Window Dressing start--%>
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody" HorizontalAlign="center" />
                                    <ItemStyle CssClass="tableBody" HorizontalAlign="Center" />
                                    <%--Window Dressing end--%>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" />
                                    <Columns>
                                        <%--<asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>--%>
                                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:RadioButton ID="rdIP" AutoPostBack="true" onclick="javascript:CheckIPSelection(this)" OnCheckedChanged="ChangeIPSettings" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="name" HeaderText="<%$ Resources:WebResources, Name%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="addressType" HeaderText="<%$ Resources:WebResources, AddTerminalEndpoint_AddressType%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="address" HeaderText="<%$ Resources:WebResources, AddTerminalEndpoint_Address%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="networkAccess" HeaderText="<%$ Resources:WebResources, NetworkAccess%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="usage" HeaderText="<%$ Resources:WebResources, Usage%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn> 
                                    </Columns>
                                 </asp:DataGrid>
                                 <asp:Label id="lblNoIPServices" cssclass="lblError" text="<%$ Resources:WebResources, AddTerminalEndpoint_lblNoIPServices%>" runat="server" visible="true"></asp:Label>
                           </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <br /><h5><font class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_ISDNServices%>" runat="server"></asp:Literal></font></h5> <%-- Organization Css Module --%>
                                 <asp:DataGrid runat="server" EnableViewState="true" ID="dgISDNServices" AutoGenerateColumns="false"
                                      CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="90%">
                                     <%--Window Dressing start--%>
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <%--Window Dressing end--%>
                                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" />
                                    <Columns>
                                        <asp:BoundColumn DataField="SortID" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="tableHeader">
                                            <ItemTemplate>
                                                <asp:RadioButton ID="rdISDN" onclick="javascript:CheckIPSelection(this)" AutoPostBack=true  OnCheckedChanged="ChangeISDNSettings" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="name" HeaderText="<%$ Resources:WebResources, Name%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="prefix" HeaderText="<%$ Resources:WebResources, Prefix%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="startRange" HeaderText="<%$ Resources:WebResources, StartRange%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="endRange" HeaderText="<%$ Resources:WebResources, EndRange%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="networkAccess" HeaderText="<%$ Resources:WebResources, NetworkAccess%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="usage" HeaderText="<%$ Resources:WebResources, Usage%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn> 
                                    </Columns>
                                 </asp:DataGrid>
                                 <asp:Label id="lblNoISDNServices" cssclass="lblError" text="<%$ Resources:WebResources, AddTerminalEndpoint_lblNoISDNServices%>" runat="server" visible="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                    <%--Window Dressing--%>
                            <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_MCUServiceAdd%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                            <td align="left" width=27% ><asp:TextBox CssClass="altText" ID="txtMCUServiceAddress" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqMCU" ControlToValidate="txtMCUServiceAddress" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic" runat="server" ValidationGroup="Submit" ></asp:RequiredFieldValidator>
                            </td>
                                    <%--Window Dressing--%>
                            <td align="left" width="22%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AddTerminalEndpoint_MCUAddressTyp%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                &nbsp<asp:DropDownList ID="lstMCUAddressType" CssClass="altLong0SelectFormat" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqMCUAT" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ControlToValidate="lstMCUAddressType" Display="dynamic" InitialValue="-1" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                            
                        </tr>
                    </table>
                </td>
             </tr>   
             <tr>
                <td align="center">
                    <%--<asp:Button ID="btnCancel" runat="server" CssClass="altShortBlueButtonFormat" Text="Cancel" OnClick="CancelEndpoint" OnClientClick="javascript:DataLoading(1);" /> <%--ZD 100176--%>
                    <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat" onserverclick="CancelEndpoint" onclick="javascript:DataLoading(1);">
					<asp:Literal Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button> <%--ZD 100420--%>
                    <%--<asp:Button ID="btnSubmit" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit/Go Back" OnClick="SubmitEndpoint" ValidationGroup="Submit"  OnClientClick="javascript:DataLoading(1);"/> <%--ZD 100176--%>
                    <button id="btnSubmit" runat="server" class="altLongBlueButtonFormat" onserverclick="SubmitEndpoint" validationgroup="Submit"  onclick="javascript:DataLoading(1);">
					<asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button> <%--ZD 100420--%>
                </td>
             </tr>
        </table>
</center>
                <input type="hidden" id="helpPage" value="29">
    </form>
    <script language="javascript">
</script>
</body>
</html>
<script type="text/javascript" src="inc/softedge.js"></script>

    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
