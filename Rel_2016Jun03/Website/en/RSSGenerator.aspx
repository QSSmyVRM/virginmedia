<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 RMT - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the RMT license.		
*
* You should have received a copy of the RMT license with	
* this file. If not, please write to: sales@RMT.com, or visit: www.RMT.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  ContentType = "text/xml" Inherits="ns_MyVRM.RSSGenerator" %>



<asp:Repeater id="Repeater1" runat="server" >

<HeaderTemplate>


<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:cf="http://www.microsoft.com/schemas/rss/core/2005">

  <channel xmlns:cfi="http://www.microsoft.com/schemas/rss/core/2005/internal" cfi:lastdownloaderror="None">

    <title cf:type="text"><%=Session["feedTitle"]%></title>
    
    <link></link>

    <description cf:type="text"></description>   

</HeaderTemplate>

<ItemTemplate>
    
  <item>

    <title xmlns:cf="http://www.microsoft.com/schemas/rss/core/2005" cf:type="text"><%# DataBinder.Eval(Container, "DataItem.Title") %></title>

    <link><%# DataBinder.Eval(Container, "DataItem.Link") %></link>    

    <description xmlns:cf="http://www.microsoft.com/schemas/rss/core/2005" cf:type="html"><%# DataBinder.Eval(Container, "DataItem.Description")%></description>
     
      <author><%# DataBinder.Eval(Container, "DataItem.ConfHostEmail")%></author>
      
      <atom:author xmlns:atom="http://www.w3.org/2005/Atom">
        
        <atom:email><%# DataBinder.Eval(Container, "DataItem.ConfHostEmail")%></atom:email>
      
      </atom:author>

       <category><%# DataBinder.Eval(Container, "DataItem.category")%></category>
       
      <guid><%# DataBinder.Eval(Container, "DataItem.GuidURL")%></guid>
       
       <pubDate><%# DataBinder.Eval(Container, "DataItem.PubDate")%></pubDate>
       
  </item>

</ItemTemplate>

<FooterTemplate>

  </channel>

</rss>    

</FooterTemplate>

</asp:Repeater>





