﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageAudioAddOnBridges" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Audio Bridges</title>

    <script type="text/javascript" src="script/mousepos.js"></script>

    <script type="text/javascript" src="script/managemcuorder.js"></script>

    <script type="text/javascript" src="inc/functions.js"></script>

    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>
   <script type="text/javascript">
       //ZD 100604 start
       var img = new Image();
       img.src = "../en/image/wait1.gif";
       //ZD 100604 End
       //ZD 100176 start
       function DataLoading(val) {
           if (val == "1")
               document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
           else
               document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
       }
       //ZD 100176 End
   </script>
    </head>
    
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div id="divbridgeheight"><%-- ZD 100393--%>
        <input type="hidden" id="helpPage" value="65" />
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, ManageAudioAddOnBridges_lblHeader%>"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV"  align="center" style="display:none">
                <img border='0' src='image/wait1.gif'  alt='Loading..' />
            </div> <%--ZD 100176--%> <%--ZD 100678 End--%>
            <tr>
                <td align="center">
                    <table width="40%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageAudioAddOnBridges_ExistingAudio%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgAudioBridges" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="false"
                        OnEditCommand="EditAudionAddonBridge" OnDeleteCommand="DeleteAudionAddonBridge"
                        Width="40%" Visible="true" Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="userID" Visible="false">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="firstName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Name%>"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="Status" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" ItemStyle-CssClass="tableBody" ItemStyle-Width="30%"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"> <%--ZD 101714--%>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageAudioAddOnBridges_btnEdit%>" id="btnEdit" visible='<%# Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("3")%>' commandname="Edit"  onclientclick="DataLoading(1)"></asp:LinkButton> <%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageAudioAddOnBridges_btnDelete%>" id="btnDelete" visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("3"))%>' commandname="Delete" onclientclick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoAudioBridges" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, NoAudiobridgesfound%>" runat="server"></asp:Literal>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="40%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td align="right" width="20%">
                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageAudioAddOnBridges_TotalAudiobri%>" runat="server"></asp:Literal></b><b><asp:Label id="lblTotalUsers" runat="server" text=""></asp:Label> </b>
                            </td>
                            <td align="right" width="20%" runat="server" id="tdLicencesRemaining">
				                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageAudioAddOnBridges_LicensesRemain%>" runat="server"></asp:Literal></b><b><asp:Label id="lblLicencesRemaining" runat="server" text=""></asp:Label> </b>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"> <%--FB 2023--%>
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server"><span class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, ManageAudioAddOnBridges_Pages%>" runat="server"></asp:Literal>:</span> </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
            </tr>
            <tr id="trNewBridge" runat="server">
                <td align="center">
                    <table width="40%" border="0">
                        <tr>
                            <td width="50%">
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:Button id="btnNewAudioBridge" runat="server" text="<%$ Resources:WebResources, ManageAudioAddOnBridges_btnNewAudioBridge%>" width="235px" onclick="CreateNewAudioAddonBridge" onclientclick="DataLoading(1)"></asp:Button> <%--FB 2796--%> <%--ZD 100176--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
   <script type="text/javascript">
       //ZD 100393 start
       if (document.getElementById("tblNoAudioBridges") != null)
           document.getElementById("divbridgeheight").style.height = "400px"
       //ZD 100393 End
   </script>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
