﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.AudioAddOnBridge" %>
<%--FB 2779 starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>
<script type="text/javascript" language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
    <script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
    <script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
    <script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
</head>
<body>
    <form id="frmAudioAddOnBridge" runat="server"><%--ZD 100176--%> 
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input id="hdnEndpointID" name="hdnWebAccUR" runat="server" value="new" type="hidden" />
    <input id="hdnUserID" name="hdnWebAccUR1" runat="server" type="hidden" />
    <input id="hdninitialTime" name="hdninitialTime" runat="server" type="hidden" />
    <div>
        <table style="width: 90%" border="0" cellpadding="5" align="center">
            <tr>
                <td align="center" colspan="5">
                    <h3>
                        <asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, AudioAddOnBridge_lblHeader%>"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5">
                    <asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" align="center" style="display :none">
               <img border='0' src='image/wait1.gif'  alt='Loading..' />
            </div> <%--ZD 100176--%><%--ZD 100678--%>
            <tr>
            <td></td>
                <td style="width: 20%" align="left" visible="false" class="blackblodtext" id="td2" runat="server"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_td2%>" runat="server"></asp:Literal></td>
                <td style="width: 30%" align="left" visible="false" id="td3" runat="server">
                    <asp:TextBox CssClass="altText" ID="txtEmailtoNotify" runat="server" onblur="CheckSecondaryEmail()"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegtxtEmailtoNotify" ControlToValidate="txtEmailtoNotify" ValidationGroup="Submit"
                        Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegtxtEmailtoNotify1" ControlToValidate="txtEmailtoNotify" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters25%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
            <td></td>
                <td style="width: 20%" align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_Name%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                </td>
                <td style="width: 30%" align="left" valign="top">
                    <asp:TextBox ID="txtBridgName" Enabled='<%# Application["ssoMode"].ToString().ToUpper().Equals("NO") %>'
                        CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqtxtBridgName" runat="server" ControlToValidate="txtBridgName"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegtxtBridgName" ControlToValidate="txtBridgName" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters49%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@#$%&'()~]*$"></asp:RegularExpressionValidator><%--ZD 103882--%>
                </td>
                <td style="width: 20%" align="left" class="blackblodtext" valign="top" rowspan="2"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_Bridgedepartme%>" runat="server"></asp:Literal></td>
                <td style="width: 30%" align="left" valign="top" rowspan="2">
                    <asp:ListBox runat="server" ID="lstBridgeDepts" CssClass="altSelectFormat" DataTextField="name"
                        DataValueField="id" Rows="6" SelectionMode="Multiple"></asp:ListBox>
					  <%--ZD 100422 Start--%>
                      <br /><span style='color: #666666;'>* <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, SelectMultiDept%>" runat="server"></asp:Literal></span>
                </td>
            </tr>
            <tr>
            <td></td>
                <td style="width: 20%" align="left" class="blackblodtext" id="Td4" runat="server" valign="top"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_Td4%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                </td>
                <td style="width: 30%" align="left" id="Td5" runat="server" valign="top">
                    <asp:DropDownList ID="lstBridgetimezone" runat="server" CssClass="altSelectFormat"
                        DataTextField="timezoneName" DataValueField="timezoneID">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstBridgetimezone" ControlToValidate="lstBridgetimezone"
                        ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic"
                        runat="server"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr id="trLblAV" runat="server" style="display: none">
                <td align="left" colspan="5">
                    <table cellspacing="5">
                        <tr>
                            <td style="width: 20">
                                &nbsp;
                            </td>
                            <td>
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_DetermineAudio%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_EndpointProfil%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                </td>
                <td align="left"><%--FB 2655 --%>
                    <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Width="250px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtEndpointName" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters17%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="ReqtxtEndpointName" runat="server" ControlToValidate="txtEndpointName"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                </td>
                <%--FB 2655 Start--%>
                 <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_EmailAddress%>" runat="server"></asp:Literal></td>
                <td align="left">
                    <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="altText" Width="250px"></asp:TextBox>
                      <asp:RegularExpressionValidator ID="regEmailAddres_1" ControlToValidate="txtEmailAddress" ValidationGroup="Submit" Display="dynamic" runat="server" 
                                    ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                      <asp:RegularExpressionValidator ID="regEmailAddres_2" ControlToValidate="txtEmailAddress" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
                <%--FB 2655 End--%>
            </tr>
            <tr>
            <td></td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_AddressType%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:DropDownList ID="lstAddressType" CssClass="altSelectFormat" DataTextField="Name"
                        DataValueField="ID" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstAddressType" ControlToValidate="lstAddressType"
                        ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic"
                        runat="server"></asp:RequiredFieldValidator>
                </td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_Address%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters28%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="ReqtxtAddress" runat="server" ControlToValidate="txtAddress"
                        ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_DefaultProtoco%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                </td>
                <td align="left">
                    <asp:DropDownList CssClass="altSelectFormat" ID="lstProtocol" runat="server" DataTextField="Name"
                        DataValueField="ID">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ReqlstProtocol" ControlToValidate="lstProtocol" ValidationGroup="Submit"
                        ErrorMessage="<%$ Resources:WebResources, Required%>" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                </td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_OutsideNetwork%>" runat="server"></asp:Literal></td>
                <td align="left">
                    <asp:DropDownList ID="lstIsOutsideNetwork" CssClass="altText" runat="server">
                        <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                        <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trMCUnumber" runat="server">
            <td></td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_InternalVideo%>" runat="server"></asp:Literal></td>
                <td align="left">
                    <asp:TextBox ID="txtIntVideoMcuNo" CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtIntVideoMcuNo" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters22%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=#$%&()'~]*$"></asp:RegularExpressionValidator> <%--ALLBUGS-90--%>
                </td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_ExternalVideo%>" runat="server"></asp:Literal></td>
                <td align="left">
                    <asp:TextBox ID="txtExtVideoMcuNo" CssClass="altText" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtExtVideoMcuNo" ValidationGroup="Submit"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters50%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|`,\[\]{}\x22;=:$%&()'~]*$"></asp:RegularExpressionValidator><%--ALLBUGS-90--%>   
                </td>
            </tr>
            <%-- ALLDEV-814 Starts --%>
            <%--<tr>
            <td></td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_LeaderPin%>" runat="server"></asp:Literal></td>
                <td align="left">
                    <asp:TextBox CssClass="altText" ID="txtLeaderPin" runat="server" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegtxtLeaderPin" ControlToValidate="txtLeaderPin"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>--%> <%--ZD 103540--%>
                <%--</td>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_ConferenceCode%>" runat="server"></asp:Literal></td>
                <td align="left">
                    <asp:TextBox CssClass="altText" ID="txtConfCode" runat="server"  MaxLength="15"></asp:TextBox>
                     <asp:RegularExpressionValidator ID="RegtxtConfCode" ControlToValidate="txtConfCode"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>--%> <%--ZD 103540--%>
                <%--</td>
            </tr>--%>            
            <%-- ALLDEV-814 Ends --%>
            <%--ZD 101446 Starts--%>
             <tr>
                <td></td>
                 <%-- ALLDEV-814 Starts --%>
                <td align="left" class="blackblodtext">
                    <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, UseUsersConferenceCode%>" runat="server"></asp:Literal>
                </td>      
                <td>
                    <asp:CheckBox ID="chkIsUserAudio" onclick="JavaScript:showhidePoolConfCode()" runat="server" />
                </td>          
                <%-- ALLDEV-814 Ends --%>
                <td colspan="2"></td>                
            </tr>
            <%--ZD 101446 Ends--%>
            <%-- ALLDEV-814 Starts --%>
            <tr id="trHPoolConfCode"  runat="server"> 
                <td colspan="5" align="left">
                    <span class="subtitleblueblodtext">
                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, AudioAddOnBridge_PoolConferenceCode%>" runat="server"></asp:Literal>
                    </span>
                </td>
            </tr>
            <tr id="trPoolConfCode" align="left" runat="server">
                <td></td>
                <td colspan="4">                    
                    <asp:DataGrid ID="dgPoolConfCode" GridLines="Horizontal" BorderColor="Blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" ViewStateMode="Enabled" runat="server">
                        <HeaderStyle CssClass="tableHeader" Height="30" />
                        <FooterStyle Height="30" />
                        <Columns>
                            <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, SNo%>">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtSNo" CssClass="altText" Width="20px" Text='<%#DataBinder.Eval(Container, "DataItem.SNo") %>' ReadOnly="true" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtprofileId" Text='<%#DataBinder.Eval(Container, "DataItem.profileId") %>' visible="false" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>   
                            <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, ParticipantCode%>">
                                <ItemTemplate>                                 
                                    <asp:TextBox CssClass="altText" ID="txtPartyCode" Text='<%#DataBinder.Eval(Container, "DataItem.ParticipantCode") %>' runat="server" MaxLength="15"> </asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegtxtPartyCode" ControlToValidate="txtPartyCode"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>"
                                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>                         
                            <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, AudioAddOnBridge_ConferenceCode%>">
                                <ItemTemplate>                                 
                                    <asp:TextBox ID="txtConfCode" CssClass="altText" Text='<%#DataBinder.Eval(Container, "DataItem.conferenceCode") %>' MaxLength="15" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ReqtxtConfCode" ControlToValidate="txtConfCode" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ValidationGroup="Submit" runat="server"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegtxtConfCode" ControlToValidate="txtConfCode" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>" Display="dynamic" SetFocusOnError="true" ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit" runat="server"></asp:RegularExpressionValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, AudioAddOnBridge_LeaderPin%>">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtLeaderPin" CssClass="altText" Text='<%#DataBinder.Eval(Container, "DataItem.leaderPin") %>' MaxLength="15" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ReqtxtLeaderPin" ControlToValidate="txtLeaderPin" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ValidationGroup="Submit" runat="server"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegtxtLeaderPin" ControlToValidate="txtLeaderPin" ErrorMessage="<%$ Resources:WebResources, AudioCodeInvalidCharacter%>" Display="dynamic" SetFocusOnError="true" ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit" runat="server"></asp:RegularExpressionValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" HeaderText="<%$ Resources:WebResources, Delete%>">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkDelete" runat="server" />                                    
                                </ItemTemplate>
                                <FooterTemplate>                                    
                                    <button ID="btnAddConfCodeOrPin" onserverclick="AddConfCodeOrPin" class="altShortBlueButtonFormat" runat="server">
									    <asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_btnAddConfCodeOrPin%>" runat="server"></asp:Literal>
                                    </button>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <%-- ALLDEV-814 Ends --%>
            <%--FB 2655 - DTMF Start--%>
            <tr>
                <td align="left" colspan="5">
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_DTMFSettings%>" runat="server"></asp:Literal></span>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_PreConference%>" runat="server"></asp:Literal></td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="PreConfCode" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regPreConfCode" ControlToValidate="PreConfCode"
                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters27%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_PreLeaderPin%>" runat="server"></asp:Literal></td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="PreLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regPreLeaderPin" ControlToValidate="PreLeaderPin"
                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters27%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
            <td></td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_PostLeaderPin%>" runat="server"></asp:Literal></td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="PostLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regPostLeaderPin" ControlToValidate="PostLeaderPin"
                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters27%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                </td>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="20%"><asp:Literal Text="<%$ Resources:WebResources, AudioAddOnBridge_AudioDialInPr%>" runat="server"></asp:Literal></td>
                <td style="text-align: left;" width="30%">
                    <asp:TextBox ID="AudioDialInPrefix" runat="server" CssClass="altText"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegAudioDialInPrefix" ControlToValidate="AudioDialInPrefix"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                    ValidationExpression="^[^<>&+]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator> <%--ZD 103540--%>
                </td>
            </tr>
            <%--FB 2655 - DTMF End--%>
            
            <tr>
              <td style="height:20px" colspan="5">
              </td>
            </tr>
            <tr>
                <td colspan="5" align="center">
                    <asp:Button runat="server" ID="btnReset" Text="<%$ Resources:WebResources, Reset%>" ValidationGroup="Reset" CssClass="altMedium0BlueButtonFormat"
                        OnClick="ResetPage" OnClientClick="javascript:DataLoading(1)" /> <%--ZD 100176--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnGoBack" Text="<%$ Resources:WebResources, AudioAddOnBridge_btnGoBack%>" CssClass="altMedium0BlueButtonFormat"
                        OnClick="GobackToParentPage" OnClientClick="javascript:DataLoading(1)" /><%--ZD 100176--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btnnewSubmi"  Text="<%$ Resources:WebResources, AudioAddOnBridge_btnnewSubmi%>"
                        ValidationGroup="Submit"  OnClientClick="javascript:return ValidateConfCode();" OnClick="SubmitNewAudioBridge" Width="300px" /> <%--FB 2796 ZD 101714 ALLDEV-814--%>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnnewSubmit" runat="server" Text="<%$ Resources:WebResources, AudioAddOnBridge_btnnewSubmit%>" ValidationGroup="Submit" 
                        OnClientClick="javascript:return ValidateConfCode();" OnClick="SubmitAudioBridge" Width="150px" /><%--FB 2796 ALLDEV-814--%>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
    //ALLDEV-814
	showhidePoolConfCode();
    
    function showhidePoolConfCode() {    
        var chkIsUserAudio = document.getElementById("chkIsUserAudio");
        var trPoolConfCode = document.getElementById("trPoolConfCode");
        var trHPoolConfCode = document.getElementById("trHPoolConfCode");

        if (chkIsUserAudio.checked) {
            trPoolConfCode.style.display = 'None';
            trHPoolConfCode.style.display = 'None';
        }
        else {
            trPoolConfCode.style.display = '';
            trHPoolConfCode.style.display = '';
        }

        var txtConfCode = "dgPoolConfCode_ctl02_txtConfCode";
        var txtLeaderPin = "dgPoolConfCode_ctl02_txtLeaderPin";

        var i = 2;
        while (document.getElementById(txtConfCode)) {
            if (chkIsUserAudio.checked) {
                if( document.getElementById(txtConfCode).value == "")
                    document.getElementById(txtConfCode).value = "ô";
                if (document.getElementById(txtLeaderPin).value == "")
                    document.getElementById(txtLeaderPin).value = "ô";
            }
            else {
                document.getElementById(txtConfCode).value = document.getElementById(txtConfCode).value.replace("ô", "");
                document.getElementById(txtLeaderPin).value = document.getElementById(txtLeaderPin).value.replace("ô", "");                
            }
            i++;
            n = i;
            if (i < 10)
                n = "0" + i;
            txtConfCode = "dgPoolConfCode_ctl" + n + "_txtConfCode";
            txtLeaderPin = "dgPoolConfCode_ctl" + n + "_txtLeaderPin";
        }
    }

    function ValidateConfCode() {    
        var chkIsUserAudio = document.getElementById("chkIsUserAudio");
        if (!chkIsUserAudio.checked) {

            if (!fnDelValidate()) {
                alert("Please enter at least one conference code details.")
                return false;
            }

            var previousConfCode = new Array();
            var count = 0;
            previousConfCode[count] = "ô";
            var isDup = false;
            $("#dgPoolConfCode tr").each(function () {
                var txtConfCode = $(this).find("input[id*='txtConfCode']");

                //if (txtConfCode.val() != "") {
                if (txtConfCode.length > 0) {

                    if (jQuery.inArray(txtConfCode.val().toLowerCase(), previousConfCode) > -1) {
                        isDup = true;
                        document.getElementById("lblError").innerText = DuplicateConferenceCode;
                        document.getElementById("lblError").innerHTML = DuplicateConferenceCode;
                        window.scrollTo(0, 0);
                    }

                    count++;
                    previousConfCode[count] = txtConfCode.val().toLowerCase();
                }
            });

            if (isDup)
                return false;
        }
        else {
            var reqFldValConfCode = document.getElementById("dgPoolConfCode_ctl02_ReqtxtConfCode");
            var reqFldValLeadPin = document.getElementById("dgPoolConfCode_ctl02_ReqtxtLeaderPin");

            ValidatorEnable(reqFldValConfCode, false);
            ValidatorEnable(reqFldValLeadPin, false);
        }
        return true;
    }

    function fnDelValidate() {

        var delCnt = 0;
        var totCnt = 0;
        var i = 2;
        var chkDelete = "dgPoolConfCode_ctl02_chkDelete";

        while (document.getElementById(chkDelete)) {

            if (document.getElementById(chkDelete).checked)
                delCnt++;
            totCnt++;
            i++;
            n = i;
            if (i < 10)
                n = "0" + i;
            chkDelete = "dgPoolConfCode_ctl" + n + "_chkDelete";
        }

        if (delCnt == totCnt) {
            return false;
        }

        return true;
    }

</script>